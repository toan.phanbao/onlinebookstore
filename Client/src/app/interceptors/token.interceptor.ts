import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NbAuthService } from '@nebular/auth';
import { mergeMap } from 'rxjs/operators';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: NbAuthService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    return this.auth.getToken()
        .pipe(
            mergeMap(val => {
                const tokenValue = val.getValue();

                request = request.clone({
                    setHeaders: {
                      Authorization: `Bearer ${tokenValue}`
                    }
                  });
                  return next.handle(request);
            })
        )
  }
}