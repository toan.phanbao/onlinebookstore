export interface GenreDto {
    id: string;
    name: string;
    description: string;
    deleted: boolean;
}

export class GenreCreateDto {
    name: string;
    description: string;

    isValid() {
        return this.name && this.description;
    }
}

export class GenreUpdateDto {
    id: string;
    name: string;
    description: string;
    deleted: boolean;

    isValid() {
        return this.name && this.description;
    }
}
