import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenreListComponent } from './genre-list/genre-list.component';
import { GenreDetailComponent } from './genre-detail/genre-detail.component';
import { GenreCreateComponent } from './genre-create/genre-create.component';
import { NbCardModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { GenreService } from 'src/app/services/genre.service';

@NgModule({
  declarations: [GenreListComponent, GenreDetailComponent, GenreCreateComponent],
  imports: [
    CommonModule,
    NbCardModule,
    Ng2SmartTableModule
    
  ],
  providers: [
    GenreService
  ]
})
export class GenreModule { }
