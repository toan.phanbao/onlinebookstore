export class GenreUrl {
    static Create = 'api/Genre/Create';

    static Update = 'api/Genre/Update';

    static Delete = 'api/Genre/Delete';

    static GetAll = 'api/Genre/GetAll';
}
