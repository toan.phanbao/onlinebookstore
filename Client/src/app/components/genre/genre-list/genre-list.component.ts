import { GenreService } from 'src/app/services/genre.service';
import { GenreCreateDto, GenreUpdateDto } from './../genre.model';
import { Component, OnInit } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/common/constant';

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styleUrls: ['./genre-list.component.css']
})
export class GenreListComponent implements OnInit {

  settings = {
    columns: {
      id: {
        title: 'ID',
        editable: false,
        addable: false
      },
      name: {
        title: 'Name'
      },
      description: {
        title: 'Description'
      },
      deleted: {
        title: 'Deleted',
        addable: false,
        filter: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
            resetText: 'clear',
          },
        },
        editor: {
          type: 'checkbox'
        }
      },
    }, // column
    pager: {
      perPage: 5
    },
    add: {
      confirmCreate: true,
      addButtonContent: '+',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>'
    }
  };

  source: ServerDataSource;

  constructor(private http: HttpClient
    , private genreService: GenreService) {
    this.source = new ServerDataSource(http,
      {
        endPoint: Constants.BaseUrl + 'api/genre/getpaging',

      }
    );
  }

  ngOnInit() { }

  onCreateConfirm(data: any) {
    const genreModel = new GenreCreateDto();
    Object.assign(genreModel, data.newData);

    if (!genreModel.isValid()) { return; }

    this.genreService.create(genreModel, result => {
      if (result.success) {
        alert('Added successfully!');
        this.reloadDatasource();
      }
    });
  }

  onEditConfirm(data: any) {
    const genreModel = new GenreUpdateDto();
    Object.assign(genreModel, data.newData);

    if (!genreModel.isValid()) { return; }

    this.genreService.update(genreModel, result => {
      if (result.success) {
        alert('Updated successfully!');
        this.reloadDatasource();
      }
    });
  }

  onDeleteConfirm(data: any) {
    const genreModel = new GenreUpdateDto();
    Object.assign(genreModel, data.data);
    genreModel.deleted = true;

    if (!genreModel.isValid()) { return; }

    this.genreService.update(genreModel, result => {
      if (result.success) {
        alert('Updated successfully!');
        this.reloadDatasource();
      }
    });
  }

  private reloadDatasource() {
    this.source.refresh();
  }

}
