import { Component, OnInit } from '@angular/core';
import { BookService } from '../../services/book.service';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent {
  constructor(private readonly _bookService: BookService) { }

  // public books: BookModel[];

  // ngOnInit() {
  //   this.getAllBooks();
  // }

  // getAllBooks() {
  //   this._bookService.getAll(result => {
  //     this.books = result;
  //     console.log(this.books);
  //   });
  // }
}
