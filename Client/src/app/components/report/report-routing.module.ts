import { LayoutComponent } from 'src/app/layout/layout.component';
import { ReportComponent } from './report.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const receiptRoutes: Routes = [
  {
    path: 'report',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: ReportComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(receiptRoutes)],
  exports: [RouterModule],
})
export class ReportRoutingModule { }
