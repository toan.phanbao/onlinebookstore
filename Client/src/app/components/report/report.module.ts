import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportComponent } from './report.component';
import { ReportService } from 'src/app/services/report.service';
import { NgxEchartsModule } from 'ngx-echarts';

@NgModule({
  declarations: [ReportComponent],
  imports: [
    CommonModule,
    NgxEchartsModule
  ],
  providers: [
    ReportService
  ]
})
export class ReportModule { }
