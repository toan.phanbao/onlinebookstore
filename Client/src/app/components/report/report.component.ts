import { Component, OnInit } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  reportData: any;
  options: any;
  currentMonth = Date.UTC;

  constructor(private reportService: ReportService) { }

  ngOnInit() {
    this.reportService.getRevenueReport(2019, result => {
      console.log('report', result);
      this.reportData = result.data;

      this.options = {
        title: {
          x: 'center',
          text: 'Revenue report',
          link: 'http://echarts.baidu.com/doc/example.html'
        },
        tooltip: {
          trigger: 'item'
        },
        toolbox: {
          show: true,
          feature: {
            dataView: { show: false, readOnly: true },
            restore: { show: false },
            saveAsImage: { show: true, title: 'Save' }
          }
        },
        calculable: true,
        grid: {
          borderWidth: 0,
          y: 80,
          y2: 60
        },
        xAxis: [
          {
            type: 'category',
            show: false,
            data: this.reportData.map(x => x.displayMonth)
          }
        ],
        yAxis: [
          {
            type: 'value',
            show: true
          }
        ],
        series: [
          {
            type: 'bar',
            itemStyle: {
              normal: {
                color: function (params) {
                  var colorList = [
                    '#C1232B', '#B5C334', '#FCCE10', '#E87C25', '#27727B',
                    '#FE8463', '#9BCA63', '#FAD860', '#F3A43B', '#60C0DD',
                    '#D7504B', '#C6E579', '#F4E001', '#F0805A', '#26C0C0'
                  ];
                  return colorList[params.dataIndex]
                },
                label: {
                  show: true,
                  position: 'bottom',
                  formatter: '{b}\n\n{c}'
                }
              }
            },
            data: this.reportData.map(x => x.total)
          }
        ]
      };
    })
  }

  
}
