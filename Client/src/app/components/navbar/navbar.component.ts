import { Component, OnInit } from '@angular/core';
// import { ImageUrl } from '../../assets/imageUrl';
import {DomSanitizer} from '@angular/platform-browser';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { NavigationService } from '../../services/navigation.service';
import { Constants } from 'src/app/common/constant';
import { NbMenuItem } from '@nebular/theme';

@Component({
    selector: 'navbar',
    templateUrl: 'navbar.component.html',
    styleUrls: ['./navbar.component.css']
})

export class NavBarComponent implements OnInit {
    // public brandImage = ImageUrl.brand;
    constructor(
      // public _domSanitizer: DomSanitizer,
      // public _navigationService: NavigationService,
      // private readonly _cookieService: CookieService
    ) {
      // const cookie = this._cookieService.getObject(Constants.UserLogin);
      // this.loginUser = cookie == null ? null : cookie.toString();
    }

    loginUser: string;

    username: "guest";

    isAuthenticated = true;

    currentVersion: string;

    ngOnInit() {

    }
}
