import { Component, OnInit } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { HttpClient } from '@angular/common/http';
import { AuthorService } from 'src/app/services/author.service';
import { Constants } from 'src/app/common/constant';
import { AuthorCreateDto, AuthorUpdateDto } from '../author.model';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit {

  settings = {
    columns: {
      id: {
        title: 'ID',
        editable: false,
        addable: false
      },
      firstName: {
        title: 'First Name'
      },
      lastName: {
        title: 'Last Name'
      },
      deleted: {
        title: 'Deleted',
        addable: false,
        filter: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
            resetText: 'clear',
          },
        },
        editor: {
          type: 'checkbox'
        }
      },
    }, // column
    pager: {
      perPage: 5
    },
    add: {
      confirmCreate: true,
      addButtonContent: '+',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>'
    }
  };

  source: ServerDataSource;

  constructor(private http: HttpClient
    , private authorService: AuthorService) {
    this.source = new ServerDataSource(http,
      {
        endPoint: Constants.BaseUrl + 'api/author/getpaging',

      }
    );
  }

  ngOnInit() { }

  onCreateConfirm(data: any) {
    const authorModel = new AuthorCreateDto();
    Object.assign(authorModel, data.newData);

    if (!authorModel.isValid()) { return; }

    this.authorService.create(authorModel, result => {
      if (result.success) {
        alert('Added successfully!');
        this.reloadDatasource();
      }
    });
  }

  onEditConfirm(data: any) {
    const authorModel = new AuthorUpdateDto();
    Object.assign(authorModel, data.newData);

    if (!authorModel.isValid()) { return; }

    this.authorService.update(authorModel, result => {
      if (result.success) {
        alert('Updated successfully!');
        this.reloadDatasource();
      }
    });
  }

  onDeleteConfirm(data: any) {
    const authorModel = new AuthorUpdateDto();
    Object.assign(authorModel, data.data);
    authorModel.deleted = true;

    if (!authorModel.isValid()) { return; }

    this.authorService.update(authorModel, result => {
      if (result.success) {
        alert('Updated successfully!');
        this.reloadDatasource();
      }
    });
  }

  private reloadDatasource() {
    this.source.refresh();
  }

}
