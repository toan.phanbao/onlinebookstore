import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorCreateComponent } from './author-create/author-create.component';
import { AuthorListComponent } from './author-list/author-list.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule } from '@nebular/theme';

@NgModule({
  declarations: [
    AuthorCreateComponent,
    AuthorListComponent
  ],
  imports: [
    CommonModule,
    Ng2SmartTableModule,
    NbCardModule
  ]
})
export class AuthorModule { }
