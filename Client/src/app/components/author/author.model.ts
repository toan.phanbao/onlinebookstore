export class AuthorModel {
  id: number;
  firstName: string;
  lastName: string;
  fullName: string;
}

export interface AuthorViewModel {
  id: string;
  fullName: string;
}

export interface AuthorFilterDto {
  firstName: string;
  lastName: string;
}

export class AuthorCreateDto {
  firstName: string;
  lastName: string;

  isValid()  {
    return this.firstName && this.lastName;
  }
}

export class AuthorUpdateDto {
  id: string;
  firstName: string;
  lastName: string;
  deleted: boolean;

  isValid()  {
    return this.firstName && this.lastName;
  }
}
