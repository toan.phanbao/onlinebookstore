export class AuthorUrls {
  static GetAll = 'api/Author/GetAll';

  static GetById = 'api/Author/Get/id';

  static Search = 'api/Author/Search';

  static Create = 'api/Author/Create';

  static Update = 'api/Author/Update';
}
