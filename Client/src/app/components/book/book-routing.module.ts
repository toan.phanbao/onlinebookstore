import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookCreateComponent } from './book-create/book-create.component';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { AuthGuard } from 'src/app/guards/auth.guard';

export const bookRoutes: Routes = [
  {
    path: 'book',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: BookListComponent
      },
      {
        path: 'book-list',
        component: BookListComponent
      },
      {
        path: 'book-create',
        component: BookCreateComponent
      },
      {
        path: 'book-detail/:guid',
        component: BookDetailComponent
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(bookRoutes)],
  exports: [RouterModule],
})
export class BookRoutingModule { }
