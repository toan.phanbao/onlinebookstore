import { Component, OnInit, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'app-book-detail-render-component',
  templateUrl: './book-detail-render-component.component.html',
  styleUrls: ['./book-detail-render-component.component.css']
})
export class BookDetailRenderComponentComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  ngOnInit() {
    this.renderValue = this.value 
      ? this.value.toString().substring(0, 6).toUpperCase() + '...'
      : 'not found';
  }

}
