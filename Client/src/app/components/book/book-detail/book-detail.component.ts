import { Component, OnInit, OnDestroy, ViewChildren } from '@angular/core';
import { BookDetailDto } from '../book.model';
import { GenreDto, GenreCreateDto } from '../../genre/genre.model';
import { AuthorViewModel, AuthorCreateDto } from '../../author/author.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GenreService } from 'src/app/services/genre.service';
import { AuthorService } from 'src/app/services/author.service';
import { CloudinaryService } from 'src/app/services/cloudinary.service';
import { BookService } from 'src/app/services/book.service';
import { ValidationService } from 'src/app/services/validation.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { NavigationService } from 'src/app/services/navigation.service';
import { NbPopoverDirective } from '@nebular/theme';
import { PublisherDto, PublisherCreateDto } from '../../publisher/publisher.model';
import { PublisherService } from 'src/app/services/publisher.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit, OnDestroy {
  @ViewChildren(NbPopoverDirective) popovers: any;
  private bookGuid: string;
  public isLoaded = false;
  private sub: Subscription;

  model: BookDetailDto;
  bookDetailForm: FormGroup;

  genres: GenreDto[];
  authors: AuthorViewModel[];
  publishers: PublisherDto[];

  isUploading = false;

  authorCreateModel: AuthorCreateDto;
  genreCreateModel: GenreCreateDto;
  publisherCreateModel: PublisherCreateDto;

  constructor(private readonly route: ActivatedRoute
    , private formBuilder: FormBuilder
    , private genreService: GenreService
    , private authorService: AuthorService
    , private cloudinaryService: CloudinaryService
    , private bookService: BookService
    , private navigationService: NavigationService
    , private publisherService: PublisherService) {

    this.model = new BookDetailDto();
    this.authorCreateModel = new AuthorCreateDto();
    this.genreCreateModel = new GenreCreateDto();
    this.publisherCreateModel = new PublisherCreateDto();

    this.bookDetailForm = this.formBuilder.group(this.initialbookDetailForm());

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.bookGuid = params['guid'];
      this.bookService.getById(this.bookGuid, result => {
        if (result.success) {
          if (result.data == null) {
            this.navigationService.goToNotFoundPage();
            return;
          }
          this.model = result.data;
          this.isLoaded = true;
        }
      })
    });

    this.loadGenre();
    this.loadAuthor();
    this.loadPublisher();
  }

  private initialbookDetailForm() {
    const bookDetailForm = {
      title: [this.model.title, [ValidationService.invalidChars, ValidationService.require('Title')]],
      authorId: [this.model.authorId, [ValidationService.require('Author')]],
      genreId: [this.model.genreId, [ValidationService.require('Genre')]],
      publisherId: [this.model.publisherId],
      publicationDate: [this.model.publicationDate],
      price: [this.model.price],
    };

    return bookDetailForm;
  }

  private loadGenre(callback?: Function) {
    this.genreService.getAll(result => {
      if (result.success) {
        this.genres = result.data;

        if (callback) {
          callback();
        }
      }
    });
  }

  private loadAuthor(callback?: Function) {
    this.authorService.getAll(result => {
      if (result.success) {
        this.authors = result.data;

        if (callback) {
          callback();
        }
      }
    });
  }

  private loadPublisher(callback?: Function) {
    this.publisherService.getAll(result => {
      if (result.success) {
        this.publishers = result.data;

        if(callback) {
          callback();
        }
      }
    });
  }

  public eventUploadFile(event: any) {
    this.isUploading = true;
    const file = event.target.files[0];

    const myReader: FileReader = new FileReader();
    myReader.readAsDataURL(file);

    myReader.onloadend = () => {

      this.cloudinaryService.upload(myReader.result)
        .subscribe(result => {
          if (result.success) {
            console.log(result);
            this.model.imageUrl = result.data.url;
          }
        });
    };
  }

  private updateBook(bookDto: BookDetailDto) {
    this.bookService.update(bookDto, result => {
      if (result.success) {
        alert('Update successfully!');
      } else {
        alert('Update failed!');
      }
    });
  }

  public eventClickUpdateBtn() {
    if (this.bookDetailForm.valid) {
      this.updateBook(this.model);
    }
    else {
      alert('Please enter correct data!')
    }
  }

  eventClickCreateAuthorBtn() {
    if (this.authorCreateModel.isValid()) {
      this.authorService.create(this.authorCreateModel, result => {
        if (result.success) {
          alert('Success');
          this.loadAuthor(() => {
            const fullname = this.authorCreateModel.firstName + ' ' + this.authorCreateModel.lastName;
            this.model.authorId = this.authors.find(x => x.fullName == fullname).id;
          });

        } else {
          alert('Fail')
        }
      });
    }
    this.closeAllPopovers();
  }

  eventClickCreateGenreBtn() {
    if (this.genreCreateModel.isValid()) {
      this.genreService.create(this.genreCreateModel, result => {
        if (result.success) {
          alert('Success');
          this.loadGenre(() => {
            const name = this.genreCreateModel.name;
            this.model.genreId = this.genres.find(x => x.name == name).id;
          });
        } else {
          alert('Fail')
        }
      });
    }
    this.closeAllPopovers();
  }

  eventClickCreatePublisherBtn() {
    if (this.publisherCreateModel.isValid()) {
      this.publisherService.create(this.publisherCreateModel, result => {
        if (result.success) {
          alert('Success');
          this.loadPublisher(() => {
            const name = this.publisherCreateModel.name;
            this.model.publisherId = this.publishers.find(x => x.name == name).id;
          });
        } else {
          alert('Fail')
        }
      });
    }
    this.closeAllPopovers();
  }

  private closeAllPopovers() {
    this.popovers.forEach(popover => {
      popover.hide();
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
