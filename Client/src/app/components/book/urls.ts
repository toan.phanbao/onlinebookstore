export class BookUrl {
  public static GetAll = 'api/Book/GetAll';

  public static GetById = 'api/Book/Get/id';

  public static Search = 'api/Book/Search';

  public static Create = 'api/Book/Create';

  public static Update = 'api/Book/Update';

  public static Delete = 'api/Book/Delete';
}
