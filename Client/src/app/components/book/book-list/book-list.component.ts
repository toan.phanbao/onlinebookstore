import { BookService } from './../../../services/book.service';
import { Component, OnInit } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/common/constant';
import { BookDetailRenderComponentComponent } from '../book-detail-render-component/book-detail-render-component.component';
import { BookDetailDto } from '../book.model';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  settings = {
    columns: {
      id: {
        title: 'ID',
        filter: false,
        type: 'custom',
        renderComponent: BookDetailRenderComponentComponent
      },
      title: {
        title: 'Title'
      },
      price: {
        title: 'Price',
        filter: false
      },
      currency: {
        title: 'Currency'
      },
      author: {
        title: 'Author'
      },
      genre: {
        title: 'Genre'
      },
      deleted: {
        title: 'Deleted',
        addable: false,
        filter: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
            resetText: 'clear',
          },
        },
        editor: {
          type: 'checkbox'
        }
      },
    }, // column
    pager: {
      perPage: 5
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>'
    },
    actions: {
      add: false,
      edit: false,
      delete: true
    }
  };

  source: ServerDataSource;

  constructor(private http: HttpClient
    , private bookService: BookService) {
    this.source = new ServerDataSource(http,
      {
        endPoint: Constants.BaseUrl + 'api/book/getpaging',
      }
    );
  }

  ngOnInit() { }

  onDeleteConfirm(data: any) {
    console.log(data);
    const bookModel = new BookDetailDto();
    Object.assign(bookModel, data.data);
    bookModel.deleted = true;

    this.bookService.delete(bookModel, result => {
      if (result.success) {
        alert('Updated successfully!');
        this.reloadDatasource();
      }
    });
  }

  private reloadDatasource() {
    this.source.refresh();
  }

}
