import { PublisherDto, PublisherCreateDto } from './../../publisher/publisher.model';
import { BookService } from './../../../services/book.service';
import { BookCreateDto } from './../book.model';
import { AuthorService } from './../../../services/author.service';
import { AuthorViewModel, AuthorCreateDto } from './../../author/author.model';
import { GenreService } from 'src/app/services/genre.service';
import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { GenreDto, GenreCreateDto } from '../../genre/genre.model';
import { CloudinaryService } from 'src/app/services/cloudinary.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ValidationService } from 'src/app/services/validation.service';
import { NbPopoverDirective } from '@nebular/theme';
import { PublisherService } from 'src/app/services/publisher.service';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css']
})
export class BookCreateComponent implements OnInit {
  @ViewChildren(NbPopoverDirective) popovers: any;

  model: BookCreateDto;
  bookCreateForm: FormGroup;

  genres: GenreDto[];
  authors: AuthorViewModel[];
  publishers: PublisherDto[];

  isUploading = false;

  authorCreateModel: AuthorCreateDto;
  genreCreateModel: GenreCreateDto;
  publisherCreateModel: PublisherCreateDto;

  constructor(private formBuilder: FormBuilder
    , private genreService: GenreService
    , private authorService: AuthorService
    , private cloudinaryService: CloudinaryService
    , private bookService: BookService
    , private publisherService: PublisherService) {

    this.model = new BookCreateDto();
    this.authorCreateModel = new AuthorCreateDto();
    this.genreCreateModel = new GenreCreateDto();
    this.publisherCreateModel = new PublisherCreateDto();

    this.bookCreateForm = this.formBuilder.group(this.initialBookCreateForm());

  }

  ngOnInit() {
    this.loadGenre();
    this.loadAuthor();
    this.loadPublisher();
  }

  private initialBookCreateForm() {
    const bookCreateForm = {
      title: [this.model.title, [ValidationService.invalidChars, ValidationService.require('Title') ]],
      authorId: [this.model.authorId, [ValidationService.require('Author') ]],
      genreId: [this.model.genreId, [ValidationService.require('Genre') ]],
      publisherId: [this.model.publisherId],
      publicationDate: [this.model.publicationDate],
      price: [this.model.price],
    };

    return bookCreateForm;
  }

  private loadGenre(callback?: Function) {
    this.genreService.getAll(result => {
      if (result.success) {
        this.genres = result.data;

        if(callback) {
          callback();
        }
      }
    });
  }

  private loadAuthor(callback?: Function) {
    this.authorService.getAll(result => {
      if (result.success) {
        this.authors = result.data;

        if(callback) {
          callback();
        }
      }
    });
  }

  private loadPublisher(callback?: Function) {
    this.publisherService.getAll(result => {
      if (result.success) {
        this.publishers = result.data;

        if(callback) {
          callback();
        }
      }
    });
  }

  public eventUploadFile(event: any) {
    this.isUploading = true;
    const file = event.target.files[0];

    const myReader: FileReader = new FileReader();
    myReader.readAsDataURL(file);

    myReader.onloadend = () => {

      this.cloudinaryService.upload(myReader.result)
        .subscribe(result => {
          if (result.success) {
            this.model.imageUrl = result.data.url;
          }
        });
    };
  }

  private createBook(bookDto: BookCreateDto) {
    this.bookService.create(bookDto, result => {
      if (result.success) {
        alert('Create successfully!');
        this.bookCreateForm.reset();
      }
    });
  }

  eventClickCreateBtn() {
    Object.keys(this.bookCreateForm.controls).forEach(field => {
      const control = this.bookCreateForm.get(field);
      
    })
    if (this.bookCreateForm.valid) {
      this.createBook(this.model);
    }
    else {
      alert('Please enter correct data!')
    }
  }

  eventClickCreateAuthorBtn() {
    if (this.authorCreateModel.isValid()) {
      this.authorService.create(this.authorCreateModel, result => {
        if (result.success) {
          alert('Success');
          this.loadAuthor(() => {
            const fullname = this.authorCreateModel.firstName + ' ' + this.authorCreateModel.lastName;
            this.model.authorId = this.authors.find(x => x.fullName == fullname).id;
          });

        } else {
          alert('Fail')
        }
      });
    }
    this.closeAllPopovers();
  }

  eventClickCreateGenreBtn() {
    if (this.genreCreateModel.isValid()) {
      this.genreService.create(this.genreCreateModel, result => {
        if (result.success) {
          alert('Success');
          this.loadGenre(() => {
            const name = this.genreCreateModel.name;
            this.model.genreId = this.genres.find(x => x.name == name).id;
          });
        } else {
          alert('Fail')
        }
      });
    }
    this.closeAllPopovers();
  }

  eventClickCreatePublisherBtn() {
    if (this.publisherCreateModel.isValid()) {
      this.publisherService.create(this.publisherCreateModel, result => {
        if (result.success) {
          alert('Success');
          this.loadPublisher(() => {
            const name = this.publisherCreateModel.name;
            this.model.publisherId = this.publishers.find(x => x.name == name).id;
          });
        } else {
          alert('Fail')
        }
      });
    }
    this.closeAllPopovers();
  }

  private closeAllPopovers() {
    this.popovers.forEach(popover => {
      popover.hide();
    });
  }
}
