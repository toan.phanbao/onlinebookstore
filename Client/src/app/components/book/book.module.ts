import { AuthorService } from './../../services/author.service';
import { BookListComponent } from './book-list/book-list.component';
import { BookComponent } from './book/book.component';
import { NgModule } from '@angular/core';
import { AuthRoutingModule } from '../auth/auth.routing';
import { BookCreateComponent } from './book-create/book-create.component';
import { NbCardModule, NbSelectModule, NbDatepickerModule, NbInputModule, NbButtonModule, NbPopoverModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CloudinaryService } from 'src/app/services/cloudinary.service';
import { ValidationMessagesComponent } from '../validation-messages/validation-messages.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { BookDetailRenderComponentComponent } from './book-detail-render-component/book-detail-render-component.component';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { NavigationService } from 'src/app/services/navigation.service';
import { RouterModule } from '@angular/router';
// import { ButtonsModule } from './buttons/buttons.module';

@NgModule({
  imports: [
    RouterModule,
    NbCardModule,
    NbSelectModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NbDatepickerModule,
    NbInputModule,
    NbButtonModule,
    Ng2SmartTableModule,
    NbPopoverModule
  ],
  declarations: [
    BookComponent,
    BookListComponent,
    BookCreateComponent,
    BookDetailComponent,
    ValidationMessagesComponent,
    BookDetailRenderComponentComponent
  ],
  entryComponents: [
    BookDetailRenderComponentComponent
  ],
  providers: [
    AuthorService,
    CloudinaryService,
    NavigationService
  ]
})
export class BookModule {

}
