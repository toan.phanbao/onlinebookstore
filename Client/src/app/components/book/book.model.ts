export interface BookDto {
  name: string;
  title: string;
  price: number | null;
  currency: string;
  publicationDate: Date | string | null;
  authorFirstName: string;
  authorLastName: string;
  genreName: string;
  publisherName: string;
}

export class BookCreateDto {
  title: string;
  price: number | null;
  currency: string;
  description: string;
  imageUrl: string;
  publicationDate: Date | string | null;
  inventoryId: string;
  authorId: string;
  genreId: string;
  publisherId: string;
}

export class BookDetailDto {
  id: string;
  title: string;
  price: number | null;
  currency: string;
  description: string;
  imageUrl: string;
  publicationDate: Date | string | null;
  inventoryId: string;
  authorId: string;
  genreId: string;
  publisherId: string;
  deleted: boolean;
}

export interface BookFilterDto {
  name: string;
  author: string;
  category: string;
}
