import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})

//TODO: Wrap up ng2-smart-table into DatatableComponent
export class DatatableComponent implements OnInit {

  @Input('settings') settings: any;
  @Input('source') source: any;

  constructor() { }

  ngOnInit() {
  }

}
