import { IssueDetailComponent } from './issue-detail/issue-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { IssueListComponent } from './issue-list/issue-list.component';
import { IssueCreateComponent } from './issue-create/issue-create.component';

export const issueRoutes: Routes = [
  {
    path: 'issue',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: IssueListComponent
      },
      {
        path: 'issue-list',
        component: IssueListComponent
      },
      {
        path: 'issue-create',
        component: IssueCreateComponent
      },
      {
        path: 'issue-detail/:guid',
        component: IssueDetailComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(issueRoutes)],
  exports: [RouterModule],
})
export class IssueRoutingModule { }
