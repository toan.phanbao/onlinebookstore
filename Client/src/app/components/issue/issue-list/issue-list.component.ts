import { Component } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { Constants } from 'src/app/common/constant';
import { HttpClient } from '@angular/common/http';
import { NavigationService } from 'src/app/services/navigation.service';
import { IssueDetailRenderComponent } from '../issue-detail-render/issue-detail-render.component';

@Component({
  selector: 'app-issue-list',
  templateUrl: './issue-list.component.html',
  styleUrls: ['./issue-list.component.css']
})
export class IssueListComponent {

  settings = {
    columns: {
      id: {
        title: 'ID',
        type: 'custom',
        renderComponent: IssueDetailRenderComponent
      },
      deliveredDate: {
        title: 'Delivered Date'
      },
      total: {
        title: 'Total'
      }
    },
    pager: {
      perPage: 5
    },
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false
    }
  };

  source: ServerDataSource;

  constructor(private http: HttpClient
    , private navigationService: NavigationService) {
    this.source = new ServerDataSource(http,
      {
        endPoint: Constants.BaseUrl + 'api/issue/getpaging',
      }
    );
  }

  eventClickCreateIssueBtn() {
    this.navigationService.goToIssueCreatePage();
  }

}
