import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NavigationService } from 'src/app/services/navigation.service';
import { ActivatedRoute } from '@angular/router';
import { IssueService } from 'src/app/services/issue.service';
import { IssueDto } from '../issue.model';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.css']
})
export class IssueDetailComponent implements OnInit {

  private issueGuid: string;
  private sub: Subscription;

  issue: IssueDto;

  constructor(private readonly route: ActivatedRoute
    , private issueService: IssueService
    , private navigationService: NavigationService)  {
    }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.issueGuid = params['guid'];
      this.issueService.getById(this.issueGuid, result => {
        if (result.success) {
          if (result.data == null) {
            this.navigationService.goToNotFoundPage();
            return;
          }
          this.issue = result.data as IssueDto;
          console.log('issue', this.issue);
        }
      });
    });
  }

  eventClickSavePdfBtn(pdf: any) {
    const filename = `issue-${this.issue.id}.pdf`;
    pdf.saveAs(filename);
  }
}
