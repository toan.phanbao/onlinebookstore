export class IssueDetailCreateDto {
    bookId: string;
    quantity: number;
    unitPrice: number;
}

export class IssueCreateDto {
    issueDetails: IssueDetailCreateDto[];
}

export interface IssueDto {
    id: string;
    total: number;
    receivedDate: string;
    issueDetails: IssueDetailDto[];
}

export interface IssueDetailDto {
    bookId: string;
    bookTitle: string;
    quantity: number;
    unitPrice: number;
}