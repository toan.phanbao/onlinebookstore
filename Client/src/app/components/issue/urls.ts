export class IssueUrl {
    static Create = 'api/Issue/Create';
    
    static GetAll = 'api/Issue/GetAll';

    static GetById = 'api/Issue/Get/{id}';
}