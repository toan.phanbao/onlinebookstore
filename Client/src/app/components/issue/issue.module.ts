import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IssueListComponent } from './issue-list/issue-list.component';
import { IssueCreateComponent } from './issue-create/issue-create.component';
import { IssueDetailComponent } from './issue-detail/issue-detail.component';
import { IssueDetailRenderComponent } from './issue-detail-render/issue-detail-render.component';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { RouterModule } from '@angular/router';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule } from '@nebular/theme';
import { NavigationService } from 'src/app/services/navigation.service';
import { IssueService } from 'src/app/services/issue.service';

@NgModule({
  declarations: [
    IssueListComponent,
    IssueCreateComponent,
    IssueDetailComponent,
    IssueDetailRenderComponent
  ],
  imports: [
    CommonModule,
    PDFExportModule,
    NbCardModule,
    Ng2SmartTableModule,
    RouterModule
  ],
  providers: [
    IssueService,
    NavigationService
  ],
  entryComponents: [
    IssueDetailRenderComponent
  ]
})
export class IssueModule { }
