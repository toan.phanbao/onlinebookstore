import { Component, OnInit, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'app-issue-detail-render',
  templateUrl: './issue-detail-render.component.html',
  styleUrls: ['./issue-detail-render.component.css']
})
export class IssueDetailRenderComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  ngOnInit() {
    this.renderValue = this.value 
      ? this.value.toString().substring(0, 6).toUpperCase() + '...'
      : 'not found';
  }
}
