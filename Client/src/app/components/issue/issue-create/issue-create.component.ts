import { NavigationService } from './../../../services/navigation.service';
import { Component, OnInit } from '@angular/core';
import { ServerDataSource, LocalDataSource } from 'ng2-smart-table';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/common/constant';
import { BookDetailRenderComponentComponent } from '../../book/book-detail-render-component/book-detail-render-component.component';
import { IssueDetailCreateDto, IssueCreateDto } from '../issue.model';
import { IssueService } from 'src/app/services/issue.service';

@Component({
  selector: 'app-issue-create',
  templateUrl: './issue-create.component.html',
  styleUrls: ['./issue-create.component.scss']
})
export class IssueCreateComponent implements OnInit {

  isShowBookList = false;

  issue: IssueCreateDto;
  issueDetails: IssueDetailCreateDto[];

  bookSettings = {
    columns: {
      id: {
        title: 'ID',
        filter: false,
        type: 'custom',
        renderComponent: BookDetailRenderComponentComponent
      },
      title: {
        title: 'Title'
      },
      author: {
        title: 'Author'
      },
      genre: {
        title: 'Genre'
      },
      deleted: {
        title: 'Deleted',
        addable: false,
        filter: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
            resetText: 'clear',
          },
        },
        editor: {
          type: 'checkbox'
        }
      },
    }, // column
    pager: {
      perPage: 5
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    }
  };

  bookSource: ServerDataSource;

  issueDetailSetting = {
    columns: {
      bookId: {
        title: 'Book ID',
        filter: false,
        editable: false,
        type: 'custom',
        renderComponent: BookDetailRenderComponentComponent
      },
      bookTitle: {
        title: 'Book Title',
        editable: false
      },
      quantity: {
        title: 'Quantity',
        editable: true
      },
      unitPrice: {
        title: 'UnitPrice',
        editable: true
      }
    },
    pager: {
      perPage: 10
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>'
    },
    actions: {
      add: false,
      edit: true,
      delete: true
    }
  };

  issueDetailSource: LocalDataSource;

  constructor(private http: HttpClient
    , private issueService: IssueService
    , private navigationService: NavigationService) {
    this.bookSource = new ServerDataSource(http,
      {
        endPoint: Constants.BaseUrl + 'api/book/getpaging',
      }
    );

    this.issueDetailSource = new LocalDataSource(this.issueDetails);
    this.issue = new IssueCreateDto();
    this.issueDetails = [];
  }

  ngOnInit() { }

  eventClickShowBtn() {
    this.isShowBookList = true;
  }

  eventBookSelect(row: any) {
    const selectedBook = {
      bookId: row.data.id,
      bookTitle: row.data.title,
      quantity: 0,
      unitPrice: 0
    };   
    
    const isBookAlreadyAdded = this.issueDetails.find(x => x.bookId == selectedBook.bookId) != null;
    
    if(isBookAlreadyAdded) {
      alert('This book has been added before!');
      return;
    }
    
    this.issueDetails.push(selectedBook as IssueDetailCreateDto);

    this.issueDetailSource = new LocalDataSource(this.issueDetails);

    alert('Added!');
    
    this.issueDetailSource.refresh();
  }

  onIssueEditConfirm(row: any) {
    // TODO: check quantity and unitPrice is number
    
    const selectedBook = {
      bookId: row.newData.bookId,
      bookTitle: row.newData.bookTitle,
      quantity: row.newData.quantity,
      unitPrice: row.newData.unitPrice
    };  
    
    const updatedBook = this.issueDetails.find(x => x.bookId == selectedBook.bookId);
    updatedBook.quantity = selectedBook.quantity;
    updatedBook.unitPrice = selectedBook.unitPrice;

    this.issueDetailSource = new LocalDataSource(this.issueDetails);
    
    this.issueDetailSource.refresh();
  }

  onIssueDeleteConfirm(row: any) {
    const selectedBookId = row.data.bookId;

    this.issueDetails = this.issueDetails.filter(x => x.bookId != selectedBookId);
    this.issueDetailSource = new LocalDataSource(this.issueDetails);

    this.issueDetailSource.refresh();
  }

  btnClickSubmitIssue() {
    this.issue.issueDetails = this.issueDetails;

    this.issueService.create(this.issue, result => {
      if (result.success) {
        alert('Create successfully');
        this.navigationService.goToIssueListPage();
      }
      
    })
  }
}
