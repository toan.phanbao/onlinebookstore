import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NbAuthComponent, NbRegisterComponent, NbLoginComponent } from '@nebular/auth';

export const routes: Routes = [
    
    {
        path: 'auth',
        component: NbAuthComponent,
        children: [
            {
                path: '',
                component: NbLoginComponent //LoginComponent
            },
            {
                path: 'login',
                component: NbLoginComponent //LoginComponent
            },
            {
                path: 'register',
                component: NbRegisterComponent
            }
        ],
        // loadChildren: './auth/auth.module#AuthModule',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {

}
