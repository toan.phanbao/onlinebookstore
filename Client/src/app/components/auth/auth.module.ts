import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NbAuthModule, NbPasswordAuthStrategy, NbAuthJWTToken } from '@nebular/auth';
import {
  NbAlertModule,
  NbButtonModule,
  NbCheckboxModule,
  NbInputModule
} from '@nebular/theme';
import { AuthRoutingModule } from './auth.routing';
import { NavigationService } from 'src/app/services/navigation.service';
import { Constants } from 'src/app/common/constant';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    AuthRoutingModule,

    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          baseEndpoint: Constants.BaseUrl,
          login: {
            endpoint: 'api/account/login',
            method: 'post',
            redirect: {
              success: '/home',
              failure: '/auth/login'
            }
          },
          register: {
            endpoint: 'api/account/register',
            method: 'post',
            redirect: {
              success: '/home',
              failure: '/auth/register'
            }
          }
        })
      ],
      forms: {},
    }),
  ],
  providers: [
    NavigationService
  ]
})
export class AuthModule {

}
