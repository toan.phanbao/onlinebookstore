import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
    selector: 'sidebar',
    templateUrl: 'sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})

export class SideBarComponent implements OnInit {
    menuItems: any[];

    items: NbMenuItem[] = [
        {
            title: 'Home',
            link: '/',
            icon: 'nb-home',
            home: true
        },

        {
            title: 'Book',
            children: [
                {
                    title: 'All Book',
                    link: '/book/book-list'
                },
                {
                    title: 'Create Book',
                    link: '/book/book-create'
                }
            ]
        },

        {
            title: 'Genre',
            link: '/genre/genre-list'
        },

        {
            title: 'Author',
            link: '/author/author-list'
        },

        {
            title: 'Publisher',
            link: '/publisher/publisher-list'
        },

        {
            title: 'Goods',
            children: [
                {
                    title: 'Receipt',
                    link: '/receipt/receipt-list'
                },
                {
                    title: 'Issue',
                    link: '/issue/issue-list'
                }
            ]
        },

        {
            title: 'Report',
            link: '/report'
        },

        {
            title: 'User',
            expanded: false,
            children: [
                {
                    title: 'User',
                    link: ''
                },
                {
                    title: 'User Role',
                    link: ''
                }
            ]
        }
    ];

    constructor() { }

    ngOnInit() {

    }
}
