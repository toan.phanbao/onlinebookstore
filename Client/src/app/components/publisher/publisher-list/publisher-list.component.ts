import { Component, OnInit } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/common/constant';
import { PublisherCreateDto, PublisherUpdateDto } from '../publisher.model';
import { PublisherService } from 'src/app/services/publisher.service';

@Component({
  selector: 'app-publisher-list',
  templateUrl: './publisher-list.component.html',
  styleUrls: ['./publisher-list.component.css']
})
export class PublisherListComponent implements OnInit {

  settings = {
    columns: {
      id: {
        title: 'ID',
        editable: false,
        addable: false
      },
      name: {
        title: 'Name'
      },
      country: {
        title: 'Country'
      },
      deleted: {
        title: 'Deleted',
        addable: false,
        filter: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
            resetText: 'clear',
          },
        },
        editor: {
          type: 'checkbox'
        }
      },
    }, // column
    pager: {
      perPage: 5
    },
    add: {
      confirmCreate: true,
      addButtonContent: '+',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>'
    }
  };

  source: ServerDataSource;

  constructor(private http: HttpClient
    , private publisherService: PublisherService) {
    this.source = new ServerDataSource(http,
      {
        endPoint: Constants.BaseUrl + 'api/publisher/getpaging',

      }
    );
  }

  ngOnInit() { }

  onCreateConfirm(data: any) {
    const publisherModel = new PublisherCreateDto();
    Object.assign(publisherModel, data.newData);

    if (!publisherModel.isValid()) { return; }

    this.publisherService.create(publisherModel, result => {
      if (result.success) {
        alert('Added successfully!');
        this.reloadDatasource();
      }
    });
  }

  onEditConfirm(data: any) {
    const publisherModel = new PublisherUpdateDto();
    Object.assign(publisherModel, data.newData);

    if (!publisherModel.isValid()) { return; }

    this.publisherService.update(publisherModel, result => {
      if (result.success) {
        alert('Updated successfully!');
        this.reloadDatasource();
      }
    });
  }

  onDeleteConfirm(data: any) {
    const publisherModel = new PublisherUpdateDto();
    Object.assign(publisherModel, data.data);
    publisherModel.deleted = true;

    if (!publisherModel.isValid()) { return; }

    this.publisherService.update(publisherModel, result => {
      if (result.success) {
        alert('Updated successfully!');
        this.reloadDatasource();
      }
    });
  }

  private reloadDatasource() {
    this.source.refresh();
  }

}
