import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublisherListComponent } from './publisher-list/publisher-list.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule } from '@nebular/theme';
import { PublisherService } from 'src/app/services/publisher.service';

@NgModule({
  declarations: [PublisherListComponent],
  imports: [
    CommonModule,
    NbCardModule,
    Ng2SmartTableModule
  ],
  providers: [
    PublisherService
  ]
})
export class PublisherModule { }
