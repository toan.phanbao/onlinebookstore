export interface PublisherDto {
    id: string;
    name: string;
    country: string;
}

export class PublisherCreateDto {
    name: string;
    country: string;

    isValid() {
        return this.name && this.country;
    }
}

export class PublisherUpdateDto {
    id: string;
    name: string;
    country: string;
    deleted: boolean;

    isValid() {
        return this.name && this.country;
    }
}