export class PublisherUrl {
    static Create = 'api/Publisher/Create';

    static Update = 'api/Publisher/Update';

    static Delete = 'api/Publisher/Delete';

    static GetAll = 'api/Publisher/GetAll';
}