export class ReceiptDetailCreateDto {
    bookId: string;
    quantity: number;
    unitPrice: number;
    // receiptId: string;
}

export class ReceiptCreateDto {
    receiptDetails: ReceiptDetailCreateDto[];
}

export interface ReceiptDto {
    id: string;
    total: number;
    receivedDate: string;
    receiptDetails: ReceiptDetailDto[];
}

export interface ReceiptDetailDto {
    bookId: string;
    bookTitle: string;
    quantity: number;
    unitPrice: number;
}