import { Component, OnInit, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'app-receipt-detail-render-component',
  templateUrl: './receipt-detail-render-component.component.html',
  styleUrls: ['./receipt-detail-render-component.component.css']
})
export class ReceiptDetailRenderComponentComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  ngOnInit() {
    this.renderValue = this.value 
      ? this.value.toString().substring(0, 6).toUpperCase() + '...'
      : 'not found';
  }

}
