import { ReceiptDetailComponent } from './receipt-detail/receipt-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { ReceiptListComponent } from './receipt-list/receipt-list.component';
import { ReceiptCreateComponent } from './receipt-create/receipt-create.component';

export const receiptRoutes: Routes = [
  {
    path: 'receipt',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: ReceiptListComponent
      },
      {
        path: 'receipt-list',
        component: ReceiptListComponent
      },
      {
        path: 'receipt-create',
        component: ReceiptCreateComponent
      },
      {
        path: 'receipt-detail/:guid',
        component: ReceiptDetailComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(receiptRoutes)],
  exports: [RouterModule],
})
export class ReceiptRoutingModule { }
