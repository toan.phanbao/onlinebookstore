import { Component } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { Constants } from 'src/app/common/constant';
import { HttpClient } from '@angular/common/http';
import { NavigationService } from 'src/app/services/navigation.service';
import { ReceiptDetailRenderComponentComponent } from '../receipt-detail-render-component/receipt-detail-render-component.component';

@Component({
  selector: 'app-receipt-list',
  templateUrl: './receipt-list.component.html',
  styleUrls: ['./receipt-list.component.css']
})
export class ReceiptListComponent {

  settings = {
    columns: {
      id: {
        title: 'ID',
        type: 'custom',
        renderComponent: ReceiptDetailRenderComponentComponent
      },
      receivedDate: {
        title: 'Received Date'
      },
      total: {
        title: 'Total'
      }
    },
    pager: {
      perPage: 5
    },
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false
    }
  };

  source: ServerDataSource;

  constructor(private http: HttpClient
    , private navigationService: NavigationService) {
    this.source = new ServerDataSource(http,
      {
        endPoint: Constants.BaseUrl + 'api/receipt/getpaging',
      }
    );
  }

  eventClickCreateReceiptBtn() {
    this.navigationService.goToReceiptCreatePage();
  }
}
