import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NavigationService } from 'src/app/services/navigation.service';
import { ActivatedRoute } from '@angular/router';
import { ReceiptService } from 'src/app/services/receipt.service';
import { ReceiptDto } from '../receipt.model';

@Component({
  selector: 'app-receipt-detail',
  templateUrl: './receipt-detail.component.html',
  styleUrls: ['./receipt-detail.component.css']
})
export class ReceiptDetailComponent implements OnInit {

  private receiptGuid: string;
  private sub: Subscription;

  receipt: ReceiptDto;

  constructor(private readonly route: ActivatedRoute
    , private receiptService: ReceiptService
    , private navigationService: NavigationService)  {
    }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.receiptGuid = params['guid'];
      this.receiptService.getById(this.receiptGuid, result => {
        if (result.success) {
          if (result.data == null) {
            this.navigationService.goToNotFoundPage();
            return;
          }
          this.receipt = result.data as ReceiptDto;
          console.log('receipt', this.receipt);
        }
      });
    });
  }

  eventClickSavePdfBtn(pdf: any) {
    const filename = `receipt-${this.receipt.id}.pdf`;
    pdf.saveAs(filename);
  }
}
