export class ReceiptUrl {
    static Create = 'api/Receipt/Create';
    
    static GetAll = 'api/Receipt/GetAll';

    static GetById = 'api/Receipt/Get/{id}';
}