import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptListComponent } from './receipt-list/receipt-list.component';
import { ReceiptService } from 'src/app/services/receipt.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule } from '@nebular/theme';
import { ReceiptCreateComponent } from './receipt-create/receipt-create.component';
import { NavigationService } from 'src/app/services/navigation.service';
import { ReceiptDetailRenderComponentComponent } from './receipt-detail-render-component/receipt-detail-render-component.component';
import { RouterModule } from '@angular/router';
import { ReceiptDetailComponent } from './receipt-detail/receipt-detail.component';

@NgModule({
  declarations: [ReceiptListComponent, ReceiptCreateComponent, ReceiptDetailRenderComponentComponent, ReceiptDetailComponent],
  imports: [
    CommonModule,
    NbCardModule,
    Ng2SmartTableModule,
    RouterModule,
    PDFExportModule
  ],
  providers: [
    ReceiptService,
    NavigationService
  ],
  entryComponents: [
    ReceiptDetailRenderComponentComponent
  ]
})
export class ReceiptModule { }
