import { NavigationService } from './../../../services/navigation.service';
import { Component, OnInit } from '@angular/core';
import { ServerDataSource, LocalDataSource } from 'ng2-smart-table';
import { HttpClient } from '@angular/common/http';
import { Constants } from 'src/app/common/constant';
import { BookDetailRenderComponentComponent } from '../../book/book-detail-render-component/book-detail-render-component.component';
import { ReceiptDetailCreateDto, ReceiptCreateDto } from '../receipt.model';
import { ReceiptService } from 'src/app/services/receipt.service';

@Component({
  selector: 'app-receipt-create',
  templateUrl: './receipt-create.component.html',
  styleUrls: ['./receipt-create.component.scss']
})
export class ReceiptCreateComponent implements OnInit {

  isShowBookList = false;

  receipt: ReceiptCreateDto;
  receiptDetails: ReceiptDetailCreateDto[];

  bookSettings = {
    columns: {
      id: {
        title: 'ID',
        filter: false,
        type: 'custom',
        renderComponent: BookDetailRenderComponentComponent
      },
      title: {
        title: 'Title'
      },
      author: {
        title: 'Author'
      },
      genre: {
        title: 'Genre'
      },
      deleted: {
        title: 'Deleted',
        addable: false,
        filter: {
          type: 'checkbox',
          config: {
            true: 'Yes',
            false: 'No',
            resetText: 'clear',
          },
        },
        editor: {
          type: 'checkbox'
        }
      },
    }, // column
    pager: {
      perPage: 5
    },
    actions: {
      add: false,
      edit: false,
      delete: false
    }
  };

  bookSource: ServerDataSource;

  receiptDetailSetting = {
    columns: {
      bookId: {
        title: 'Book ID',
        filter: false,
        editable: false,
        type: 'custom',
        renderComponent: BookDetailRenderComponentComponent
      },
      bookTitle: {
        title: 'Book Title',
        editable: false
      },
      quantity: {
        title: 'Quantity',
        editable: true
      },
      unitPrice: {
        title: 'UnitPrice',
        editable: true
      }
    },
    pager: {
      perPage: 10
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>'
    },
    actions: {
      add: false,
      edit: true,
      delete: true
    }
  };

  receiptDetailSource: LocalDataSource;

  constructor(private http: HttpClient
    , private receiptService: ReceiptService
    , private navigationService: NavigationService) {
    this.bookSource = new ServerDataSource(http,
      {
        endPoint: Constants.BaseUrl + 'api/book/getpaging',
      }
    );

    this.receiptDetailSource = new LocalDataSource(this.receiptDetails);
    this.receipt = new ReceiptCreateDto();
    this.receiptDetails = [];
  }

  ngOnInit() { }

  eventClickShowBtn() {
    this.isShowBookList = true;
  }

  eventBookSelect(row: any) {
    const selectedBook = {
      bookId: row.data.id,
      bookTitle: row.data.title,
      quantity: 0,
      unitPrice: 0
    };   
    
    const isBookAlreadyAdded = this.receiptDetails.find(x => x.bookId == selectedBook.bookId) != null;
    
    if(isBookAlreadyAdded) {
      alert('This book has been added before!');
      return;
    }
    
    this.receiptDetails.push(selectedBook as ReceiptDetailCreateDto);

    this.receiptDetailSource = new LocalDataSource(this.receiptDetails);

    alert('Added!');
    
    this.receiptDetailSource.refresh();
  }

  onReceiptEditConfirm(row: any) {
    // TODO: check quantity and unitPrice is number
    
    const selectedBook = {
      bookId: row.newData.bookId,
      bookTitle: row.newData.bookTitle,
      quantity: row.newData.quantity,
      unitPrice: row.newData.unitPrice
    };  
    
    const updatedBook = this.receiptDetails.find(x => x.bookId == selectedBook.bookId);
    updatedBook.quantity = selectedBook.quantity;
    updatedBook.unitPrice = selectedBook.unitPrice;

    this.receiptDetailSource = new LocalDataSource(this.receiptDetails);
    
    this.receiptDetailSource.refresh();
  }

  onReceiptDeleteConfirm(row: any) {
    const selectedBookId = row.data.bookId;

    this.receiptDetails = this.receiptDetails.filter(x => x.bookId != selectedBookId);
    this.receiptDetailSource = new LocalDataSource(this.receiptDetails);

    this.receiptDetailSource.refresh();
  }

  btnClickSubmitReceipt() {
    this.receipt.receiptDetails = this.receiptDetails;

    this.receiptService.create(this.receipt, result => {
      if (result.success) {
        alert('Create successfully');
        this.navigationService.goToReceiptListPage();
      }
      
    })
  }
}
