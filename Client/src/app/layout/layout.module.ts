import { NgModule } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';

@NgModule({
  imports: [
    
  ],
  providers: [NbSidebarService]
})
export class LayoutModule {

}