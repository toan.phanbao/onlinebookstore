// import { Injectable } from '@angular/core';
// import { NbAuthResult, NbTokenService, NbAuthSimpleToken, NbAuthJWTToken } from '@nebular/auth';
// import { map } from 'rxjs/operators';
// import { of as observableOf, Observable, of } from 'rxjs';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Constants } from '../common/constant';

// @Injectable()
// export class AuthService {
//   private readonly baseUrl = Constants.BaseUrl;

//   constructor(private http: HttpClient, private tokenService: NbTokenService) {
//   }

//   authenticate(loginModel: ILoginModel, callback: Function): void {
//     const token = localStorage.getItem(Constants.Jwt);

//     let options: {
//       headers: HttpHeaders
//     };

//     options = {
//       headers: new HttpHeaders({
//         'Content-Type': 'application/json; charset=utf-8',
//         Authorization: `Bearer ${token}`
//       })
//     };

//     this.http.post(this.baseUrl + 'api/Account/Login', loginModel, options)
//       .pipe(
//         map((res: IAuthResult) => {
//           debugger
//           if (!res.success) {
//             alert('error');
//             return res;
//           }

//           const nbAuthResult = new NbAuthResult(
//             res.success,
//             res.response,
//             'home',
//             res.error,
//             res.messages,
//             new NbAuthJWTToken(
//               res.token.token,
//               res.token.ownerStrategyName,
//               new Date(res.token.createdAt)
//             )
//           )
//           this.processResultToken(nbAuthResult);

//           return res;
//         })
//       )
//       .subscribe(result => {
//         callback(result);
//       });
//   }

//   private processResultToken(result: NbAuthResult): Observable<NbAuthResult> {
//     if (result.isSuccess() && result.getToken()) {
//       return this.tokenService.set(result.getToken())
//         .pipe(
//           map(() => {
//             return result;
//           }),
//         );
//     }

//     return observableOf(result);
//   }
// }

// interface IAuthResult {
//   success: boolean;
//   response: string;
//   redirect: string;
//   messages: string[];
//   error: string;
//   token: any;
// }