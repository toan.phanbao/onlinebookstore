import { ReceiptUrl } from './../components/receipt/receipt.url';

import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ReceiptCreateDto } from '../components/receipt/receipt.model';

@Injectable()
export class ReceiptService {
  constructor(private api: ApiService) {

  }

  create(receiptModel: ReceiptCreateDto, callback?: any) {
    this.api.post(ReceiptUrl.Create, receiptModel)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  getAll(callback?: any) {
    this.api.get(ReceiptUrl.GetAll)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  getById(id: string, callback?: any) {
    this.api.get(ReceiptUrl.GetById.replace('{id}', id))
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }
}
