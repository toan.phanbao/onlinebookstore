import { BookDetailDto } from './../components/book/book.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BookUrl } from '../components/book/urls';
import { BookCreateDto } from '../components/book/book.model';

@Injectable()
export class BookService {

  constructor(private readonly _apiService: ApiService) { }

  getById(id: string, callback?: Function) {
    this._apiService.get(BookUrl.GetById.replace('id', id), null)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  create(book: BookCreateDto, callback?: Function) {
    this._apiService.post(BookUrl.Create, book)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  update(book: BookDetailDto, callback?: Function) {
    this._apiService.post(BookUrl.Update, book)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  delete(book: BookDetailDto, callback?: Function) {
    this._apiService.post(BookUrl.Delete, book)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }
}
