import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class NavigationService {

  constructor(
    private readonly router: Router
  ) { }

  goToHome() {
    this.router.navigateByUrl('/home');
  }

  goToLoginPage() {
    this.router.navigateByUrl('auth/login');
  }

  goToNotFoundPage() {
    this.router.navigateByUrl('miscellaneous/404');
  }

  goToUnauthorizePage() {
    this.router.navigateByUrl('miscellaneous/unauthorize');
  }

  goToReceiptCreatePage() {
    this.router.navigateByUrl('receipt/receipt-create');
  }

  goToReceiptListPage() {
    this.router.navigateByUrl('receipt/receipt-list');
  }

  goToIssueCreatePage() {
    this.router.navigateByUrl('issue/issue-create');
  }

  goToIssueListPage() {
    this.router.navigateByUrl('issue/issue-list');
  }
}
