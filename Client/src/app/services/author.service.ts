import { AuthorUpdateDto } from './../components/author/author.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { AuthorUrls } from '../components/author/urls';
import { AuthorCreateDto } from '../components/author/author.model';

@Injectable()
export class AuthorService {

  constructor(private readonly api: ApiService) { }

  getAll(callback?: any) {
    this.api.get(AuthorUrls.GetAll, null)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  create(authorModel: AuthorCreateDto, callback?: any) {
    this.api.post(AuthorUrls.Create, authorModel, null)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  update(authorModel: AuthorUpdateDto, callback?: any) {
    this.api.post(AuthorUrls.Update, authorModel, null)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }
}
