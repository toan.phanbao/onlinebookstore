import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';


@Injectable()
export class CloudinaryService {
  private cloudUrl = "https://api.cloudinary.com/v1_1/toanphanbao/image/upload";
  private upload_preset = "g7pgnocm"; // config on Cloudinary

  private http: HttpClient;
  constructor(private handler: HttpBackend) {
    this.http = new HttpClient(handler);
  }

  public upload(file: any) {
    return this.http.post(
      this.cloudUrl,
      {
        file: file,
        upload_preset: this.upload_preset
      }
    ).pipe(
      map((res: any) => {
        return this.handleSuccess(res);
      }),
      catchError((error: any) => {
        return this.handleError(error);
      }));
  }

  protected handleSuccess(success: any) {
    const result: IExecutionResult = {
      success: true,
      data: success
    };

    return result;
  }

  protected handleError(error: any) {
    console.log(error);

    const errorResult: IExecutionResult = {
      success: false,
      data: null
    };

    return of(errorResult);
  }
}

interface IExecutionResult {
  success: boolean;
  data: any;
}