import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ReportUrl } from '../components/report/urls';

@Injectable()
export class ReportService {
    constructor(private api: ApiService) { }

    getRevenueReport(year: number, callback?: Function) {
        this.api.get(ReportUrl.GetRevenueReport.replace('{year}', year.toString()))
        .subscribe(result => {
            if (callback) {
              callback(result);
            }
          });
    }
}