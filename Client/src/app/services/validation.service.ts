import { FormControl } from '@angular/forms';
import { htmlEncode } from 'js-htmlencode';

export class ValidationService {
    static require(fieldName: string) {
        return (control: FormControl): { [key: string]: any } => {
            if (control.value == null || control.value === '' || typeof control.value === 'string' && !control.value.trim()) {
                return { 'required': fieldName };
            }
            return null;
        };
    }

    static requireForPassword(fieldName: string) {
        return (control: FormControl): { [key: string]: any } => {
            if (control.value == null || control.value === '') {
                return { 'required': fieldName };
            }
            return null;
        };
    }

    static requireForCheckbox(fieldName: string) {
        return (control: FormControl): { [key: string]: any } => {
            if (!control.value) {
                return { 'required' : fieldName };
            }
            return null;
        };
    }

    static replaceSpecialChars(str, chars) {
        chars = typeof chars === 'string' ? [chars] : chars;
        if (!Array.isArray(chars)) {
            return null;
        }

        // the special chars will be replaced with html symbols
        const list = {
            '%': '&#37;'
        };
        chars.forEach(function (c) {
            if (list.hasOwnProperty(c)) {
                str = str.replace(new RegExp(c, 'gi'), list[c]);
          }
        });
        return str;
    }

    static checkInputHaveSpecialChar(inputValue) {
        if (inputValue == null || inputValue === '') { return false; }
        let encodeInput =  htmlEncode(inputValue);
        encodeInput = ValidationService.replaceSpecialChars(encodeInput, '%');
        return encodeInput === inputValue;
    }

    static invalidChars(control: FormControl) {
        if (control.value && !ValidationService.checkInputHaveSpecialChar(control.value)) {
            control.markAsTouched();
            return { 'invalid_chars': '' };
        }
        return null;
    }

    static checkValidFormatDateNotInTheFuture(control: FormControl) {
        const dateInput = control.value;
        if (dateInput == null && control.errors != null && control.errors.hasOwnProperty('invalid_format_completiondate')) {
            return { 'invalid_format_completiondate': '' };
        }
        if (dateInput != null) {
            const now = new Date(new Date().setHours(0, 0, 0));
            const inputDate = new Date(dateInput.setHours(0, 0, 0));
            if (now < inputDate) {
                control.markAsTouched();
                return { 'date-received-is-future': '' };
            }
        }
        return null;
    }

    static requiredForAutoCompleteControl(fieldName: string) {
        return (control: FormControl): { [key: string]: any } => {
            // keep require error until it checked
            if (control.hasError('required') || control.hasError('custom_error')) {
                return { 'required': fieldName };
            }
            return null;
        };
    }

    static getErrorMessage(validatorName: string, validatorValue?: any) {
        const config = {
            'required': validatorValue + ' is required.',
            'minlength': `Minimum length ${validatorValue.requiredLength}.`,
            'invalid_chars': 'Invalid characters.',
        };

        return config[validatorName];
    }
}
