import { GenreUrl } from './../components/genre/genre.url';

import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { GenreCreateDto } from '../components/genre/genre.model';

@Injectable()
export class GenreService {
  constructor(private api: ApiService) {

  }

  create(genreModel: GenreCreateDto, callback?: any) {
    this.api.post(GenreUrl.Create, genreModel)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  update(genreModel: GenreCreateDto, callback?: any) {
    this.api.post(GenreUrl.Update, genreModel)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  getAll(callback?: any) {
    this.api.get(GenreUrl.GetAll)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }
}
