import { Injectable } from '@angular/core';
import { AuthorService } from './author.service';
import { AuthorModel } from '../components/author/author.model';

@Injectable()
export class AuthorHelperService {

  constructor(private readonly _authorService: AuthorService) { }

  public authors: AuthorModel[];

  getAuthorList() {
    this._authorService.getAll(result => {
      this.authors = result;
    });
  }

  updateAuthorList() {
    this.authors = null;
    this.getAuthorList();
  }
}
