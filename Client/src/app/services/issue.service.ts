import { IssueUrl } from './../components/issue/urls';

import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { IssueCreateDto } from '../components/issue/issue.model';

@Injectable()
export class IssueService {
  constructor(private api: ApiService) {

  }

  create(issueModel: IssueCreateDto, callback?: any) {
    this.api.post(IssueUrl.Create, issueModel)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  getAll(callback?: any) {
    this.api.get(IssueUrl.GetAll)
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }

  getById(id: string, callback?: any) {
    this.api.get(IssueUrl.GetById.replace('{id}', id))
      .subscribe(result => {
        if (callback) {
          callback(result);
        }
      });
  }
}
