import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../common/constant';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) { }

  private readonly baseUrl = Constants.BaseUrl;

  public get(url: string, params?: any, callback?: any) {
    const token = localStorage.getItem(Constants.Jwt);

    const headers = new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`
      });

    const result = this.http
      .get(this.baseUrl + url,
        {
          headers: headers,
          params: params
        })
      .pipe(
        map((res: any) => {
          return this.handleSuccess(res);
        }),
        catchError((error: any) => {
          return this.handleError(error);
        }));

    return result;
  }

  public post(url: string, body?: any, options?: any) {
    const token = localStorage.getItem(Constants.Jwt);

    options = options || {};
    options.headers =
      options.headers ||
      new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        Authorization: `Bearer ${token}`
      });

    const result = this.http.post(this.baseUrl + url, body, options)
      .pipe(
        map((res: any) => {
          return this.handleSuccess(res);
        }),
        catchError((error: any) => {
          return this.handleError(error);
        }));

    return result;
  }

  protected handleResult(callback) {
    const observer = {
      next: (x: any) => this.handleSuccess(x),
      error: (e: any) => this.handleError(e)
    };

    return observer;
  }

  protected handleSuccess(success: any) {
    const result: IExecutionResult = {
      success: true,
      message: success.message,
      data: success.data
    };

    return result;
  }

  protected handleError(error: any) {
    console.log(error);

    const errorResult: IExecutionResult = {
      success: false,
      message: error.message,
      data: null
    };

    return of(errorResult);
  }
}

interface IExecutionResult {
  success: boolean;
  message: string;
  data: any;
}
