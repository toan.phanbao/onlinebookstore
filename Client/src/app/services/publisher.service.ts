import { Injectable } from '@angular/core';
import { PublisherCreateDto } from '../components/publisher/publisher.model';
import { PublisherUrl } from '../components/publisher/publisher.url';
import { ApiService } from './api.service';

@Injectable()
export class PublisherService {

    constructor(private api: ApiService) {
    }
  
    create(PublisherModel: PublisherCreateDto, callback?: any) {
      this.api.post(PublisherUrl.Create, PublisherModel)
        .subscribe(result => {
          if (callback) {
            callback(result);
          }
        });
    }
  
    update(PublisherModel: PublisherCreateDto, callback?: any) {
      this.api.post(PublisherUrl.Update, PublisherModel)
        .subscribe(result => {
          if (callback) {
            callback(result);
          }
        });
    }
  
    getAll(callback?: any) {
      this.api.get(PublisherUrl.GetAll)
        .subscribe(result => {
          if (callback) {
            callback(result);
          }
        });
    }
}