import { MiscellaneousComponent } from './miscellaneous/miscellaneous.component';
import { LayoutComponent } from './layout/layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { GenreListComponent } from './components/genre/genre-list/genre-list.component';
import { AuthorListComponent } from './components/author/author-list/author-list.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { PublisherListComponent } from './components/publisher/publisher-list/publisher-list.component';
import { NbThemeModule } from '@nebular/theme';
import { AuthRoutingModule } from './components/auth/auth.routing';
import { ReceiptRoutingModule } from './components/receipt/receipt-routing.module';
import { BookRoutingModule } from './components/book/book-routing.module';
import { AuthGuard } from './guards/auth.guard';
import { UnauthorizeComponent } from './miscellaneous/unauthorize/unauthorize.component';
import { IssueRoutingModule } from './components/issue/issue-routing.module';
import { ReportRoutingModule } from './components/report/report-routing.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'prefix'
  },
  {
    path: 'home',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      }
    ]
  },

  // {
  //   path: 'book',
  //   canActivate: [AuthGuard],
  //   loadChildren: 'src/app/components/book/book-routing.module#BookRoutingModule'
  // },

  {
    path: 'genre',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: GenreListComponent
      },
      {
        path: 'genre-list',
        component: GenreListComponent
      }
    ]
  },

  {
    path: 'author',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: AuthorListComponent
      },
      {
        path: 'author-list',
        component: AuthorListComponent
      }
    ]
  },

  {
    path: 'publisher',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: PublisherListComponent
      },
      {
        path: 'publisher-list',
        component: PublisherListComponent
      }
    ]
  },

  {
    path: 'miscellaneous',
    component: MiscellaneousComponent,
    children: [
      {
        path: '404',
        component: NotFoundComponent
      },
      {
        path: 'unauthorize',
        component: UnauthorizeComponent
      }
    ]
  },
  
  {
    path: '**',
    redirectTo: 'miscellaneous/404'
  }
];

@NgModule({
  imports: [
    AuthRoutingModule,
    BookRoutingModule,
    ReceiptRoutingModule,
    IssueRoutingModule,
    ReportRoutingModule,

    NbThemeModule.forRoot({ name: 'default' }),
    
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
