import { map } from 'rxjs/operators';
import { NavigationService } from 'src/app/services/navigation.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { NbAuthService } from '@nebular/auth';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authService: NbAuthService
        , private navigationService: NavigationService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.canDo();
    }

    private canDo() {
        return this.authService.getToken().pipe(
            map(token => {
                if (token.isValid()) {
                    return true;
                }

                this.navigationService.goToUnauthorizePage();
                return false;
            })
        )
    }
}