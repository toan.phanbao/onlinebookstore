import { Component } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-unauthorize',
  templateUrl: './unauthorize.component.html',
  styleUrls: ['./unauthorize.component.scss']
})
export class UnauthorizeComponent {

  constructor(private navigationService: NavigationService) {
  }

  goToLoginPage() {
    this.navigationService.goToLoginPage();
  }

}
