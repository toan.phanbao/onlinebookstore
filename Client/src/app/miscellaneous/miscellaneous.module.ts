import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MiscellaneousComponent } from './miscellaneous.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterModule } from '@angular/router';
import { NavigationService } from '../services/navigation.service';
import { UnauthorizeComponent } from './unauthorize/unauthorize.component';

@NgModule({
  declarations: [
    MiscellaneousComponent,
    NotFoundComponent,
    UnauthorizeComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  providers:[
    NavigationService
  ]
})
export class MiscellaneousModule { }
