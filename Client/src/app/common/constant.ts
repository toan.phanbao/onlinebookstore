export class Constants {
  public static readonly UserLogin = 'userLogin';
  public static readonly Jwt = 'jwt';

  // public static BaseUrl = "http://localhost:5230"; // vs code
  public static readonly BaseUrl = 'https://localhost:44387/'; // vs

}
