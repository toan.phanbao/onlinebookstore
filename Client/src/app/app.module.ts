import { HomeComponent } from './components/home/home.component';
import { ApiService } from './services/api.service';
import { BookService } from './services/book.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { NbLayoutModule
  , NbSidebarModule
  , NbSearchModule
  , NbMenuModule
  , NbCardModule
  , NbDatepickerModule } from '@nebular/theme';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LayoutComponent } from './layout/layout.component';
import { LayoutModule } from './layout/layout.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SideBarComponent } from './components/sidebar/sidebar.component';
import { AuthModule } from './components/auth/auth.module';
import { NavBarComponent } from './components/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BookModule } from './components/book/book.module';
import { DatatableComponent } from './components/datatable/datatable.component';
import { AuthorModule } from './components/author/author.module';
import { PublisherModule } from './components/publisher/publisher.module';
import { GenreModule } from './components/genre/genre.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { ReceiptModule } from './components/receipt/receipt.module';
import { AuthGuard } from './guards/auth.guard';
import { IssueModule } from './components/issue/issue.module';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ReportModule } from './components/report/report.module';



@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HomeComponent,
    SideBarComponent,
    NavBarComponent,
    DatatableComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    BookModule,
    GenreModule,
    PublisherModule,
    ReceiptModule,
    IssueModule,
    MiscellaneousModule,
    ReportModule,

    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    LayoutModule,
    NbLayoutModule,
    NbSidebarModule,
    NbSearchModule,
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    AuthModule,
    BookModule,
    AuthorModule,
    PublisherModule,
    NbCardModule,
    PDFExportModule
  ],
  providers: [
    ApiService,
    BookService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
