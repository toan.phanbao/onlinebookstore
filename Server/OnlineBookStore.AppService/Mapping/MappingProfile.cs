﻿using System;
using System.Linq;
using AutoMapper;

using OnlineBookStore.AppService.Domains;
using OnlineBookStore.AppService.DTOs.Author;
using OnlineBookStore.AppService.DTOs.Book;
using OnlineBookStore.AppService.DTOs.Genre;
using OnlineBookStore.AppService.DTOs.Issue;
using OnlineBookStore.AppService.DTOs.Publisher;
using OnlineBookStore.AppService.DTOs.Receipt;
using OnlineBookStore.Common.Constants;

namespace OnlineBookStore.AppService.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Book

            CreateMap<BookEntity, BookDto>()
                .ForMember(des => des.Author,
                    opt => opt.MapFrom(src => $"{src.Author.FirstName} {src.Author.LastName}"))
                .ForMember(des => des.Genre,
                    opt => opt.MapFrom(src => src.Genre.Name))
                .ForMember(des => des.Publisher,
                    opt => opt.MapFrom(src => src.Publisher.Name));

            CreateMap<BookCreateDto, BookEntity>();
            CreateMap<BookEntity, BookDetailDto>();

            CreateMap<BookDetailDto, BookEntity>();

            #endregion


            #region Genre

            CreateMap<GenreEntity, GenreDto>();

            CreateMap<GenreCreateDto, GenreEntity>()
                .ForMember(des => des.Id, opt => opt.Ignore());

            CreateMap<GenreUpdateDto, GenreEntity>();

            #endregion


            #region Author
            CreateMap<AuthorEntity, AuthorDto>()
                .ForMember(des => des.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(des => des.LastName, opt => opt.MapFrom(src => src.LastName));

            CreateMap<AuthorEntity, AuthorViewDto>()
                .ForMember(des => des.FullName, opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"))
                .ForMember(des => des.Id, opt => opt.MapFrom(src => src.Id));

            CreateMap<AuthorCreateDto, AuthorEntity>()
                .ForMember(des => des.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(des => des.LastName, opt => opt.MapFrom(src => src.LastName));

            CreateMap<AuthorUpdateDto, AuthorEntity>()
                .ForMember(des => des.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(des => des.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(des => des.Deleted, opt => opt.MapFrom(src => src.Deleted));

            #endregion

            #region Publisher

            CreateMap<PublisherEntity, PublisherDto>();

            CreateMap<PublisherCreateDto, PublisherEntity>()
                .ForMember(des => des.Id, opt => opt.Ignore());

            CreateMap<PublisherUpdateDto, PublisherEntity>();

            #endregion


            #region Receipt

            
            CreateMap<ReceiptDetailDto, ReceiptDetailEntity>();

            CreateMap<ReceiptCreateDto, ReceiptEntity>()
                .ForMember(des => des.Id, opt => opt.Ignore())
                .ForMember(des => des.ReceivedDate, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(des => des.Total,
                    opt => opt.MapFrom(src => src.ReceiptDetails.Sum(x => x.Quantity * x.UnitPrice)));

            CreateMap<ReceiptDetailDto, ReceiptDetailEntity>()
                .ForMember(des => des.Id, opt => opt.Ignore());

            CreateMap<ReceiptEntity, ReceiptDto>()
                .ForMember(des => des.ReceivedDate,
                    opt => opt.MapFrom(src => src.ReceivedDate.ToString(DateTimeFormat.Default)));

            CreateMap<ReceiptEntity, ReceiptViewDto>()
                .ForMember(des => des.ReceivedDate,
                    opt => opt.MapFrom(src => src.ReceivedDate.ToString(DateTimeFormat.Default)));

            CreateMap<ReceiptDetailEntity, ReceiptDetailViewDto>()
                .ForMember(des => des.BookTitle,
                    opt => opt.MapFrom(src => src.Book.Title));



            #endregion


            #region Issue

            
            CreateMap<IssueDetailDto, IssueDetailEntity>();

            CreateMap<IssueCreateDto, IssueEntity>()
                .ForMember(des => des.Id, opt => opt.Ignore())
                .ForMember(des => des.DeliveredDate, opt => opt.MapFrom(src => DateTime.UtcNow))
                .ForMember(des => des.Total,
                    opt => opt.MapFrom(src => src.IssueDetails.Sum(x => x.Quantity * x.UnitPrice)));

            CreateMap<IssueDetailDto, IssueDetailEntity>()
                .ForMember(des => des.Id, opt => opt.Ignore());

            CreateMap<IssueEntity, IssueDto>()
                .ForMember(des => des.ReceivedDate,
                    opt => opt.MapFrom(src => src.DeliveredDate.ToString(DateTimeFormat.Default)));

            CreateMap<IssueEntity, IssueViewDto>()
                .ForMember(des => des.ReceivedDate,
                    opt => opt.MapFrom(src => src.DeliveredDate.ToString(DateTimeFormat.Default)));

            CreateMap<IssueDetailEntity, IssueDetailViewDto>()
                .ForMember(des => des.BookTitle,
                    opt => opt.MapFrom(src => src.Book.Title));



            #endregion
        }
    }
}