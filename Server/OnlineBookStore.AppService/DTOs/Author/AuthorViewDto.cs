﻿using System;

namespace OnlineBookStore.AppService.DTOs.Author
{
    public class AuthorViewDto
    {
        public Guid Id { get; set; }

        public string FullName { get; set; }
    }
}