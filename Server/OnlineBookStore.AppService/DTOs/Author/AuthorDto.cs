﻿using System;

namespace OnlineBookStore.AppService.DTOs.Author
{
    public class AuthorDto
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Deleted { get; set; }
    }
}
