﻿namespace OnlineBookStore.AppService.DTOs.Author
{
    public class AuthorCreateDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
