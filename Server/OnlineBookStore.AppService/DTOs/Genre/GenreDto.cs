﻿using System;

namespace OnlineBookStore.AppService.DTOs.Genre
{
    public class GenreDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Deleted { get; set; }
    }
}