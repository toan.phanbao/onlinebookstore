﻿using System.ComponentModel.DataAnnotations;

namespace OnlineBookStore.AppService.DTOs.Genre
{
    public class GenreCreateDto
    {
        [Required]
        [MaxLength(512)]
        public string Name { get; set; }

        [Required]
        [MaxLength(512)]
        public string Description { get; set; }
    }
}