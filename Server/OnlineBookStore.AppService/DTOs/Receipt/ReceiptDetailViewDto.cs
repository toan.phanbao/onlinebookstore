using System;

namespace OnlineBookStore.AppService.DTOs.Receipt
{
    public class ReceiptDetailViewDto
    {
        public Guid BookId { get; set; }

        public string BookTitle { get; set; }

        public long Quantity { get; set; }

        public decimal UnitPrice { get; set; }
    }
}