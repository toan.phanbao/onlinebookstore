using System;
using System.Collections.Generic;

namespace OnlineBookStore.AppService.DTOs.Receipt
{
    public class ReceiptViewDto
    {
        public Guid Id { get; set; }

        public decimal Total { get; set; }

        public string ReceivedDate { get; set; }

        public List<ReceiptDetailViewDto> ReceiptDetails { get; set; }
    }
}