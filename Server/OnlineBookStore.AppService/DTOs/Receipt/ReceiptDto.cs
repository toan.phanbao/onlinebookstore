﻿using System;

namespace OnlineBookStore.AppService.DTOs.Receipt
{
    public class ReceiptDto
    {
        public Guid Id { get; set; }

        public decimal Total { get; set; }

        public string ReceivedDate { get; set; }

    }
}