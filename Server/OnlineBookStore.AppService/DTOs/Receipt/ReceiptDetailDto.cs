﻿using System;

namespace OnlineBookStore.AppService.DTOs.Receipt
{
    public class ReceiptDetailDto
    {
        public Guid BookId { get; set; }

        public long Quantity { get; set; }

        public Guid ReceiptId { get; set; }

        public decimal UnitPrice { get; set; }
    }
}