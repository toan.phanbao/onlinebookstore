﻿using System.Collections.Generic;

namespace OnlineBookStore.AppService.DTOs.Receipt
{
    public class ReceiptCreateDto
    {
        public List<ReceiptDetailDto> ReceiptDetails { get; set; }
    }
}