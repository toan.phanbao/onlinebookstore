namespace OnlineBookStore.AppService.DTOs.Receipt
{
    public interface IReportDto
    {
        int Month {get; set;}
        string DisplayMonth {get; set;}
        decimal Total {get; set;}
    }
}