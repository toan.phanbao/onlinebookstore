﻿namespace OnlineBookStore.AppService.DTOs.Configuration
{
    public class ConnectionStringDto
    {
        public string UserContext { get; set; }

        public string DataContext { get; set; }
    }
}
