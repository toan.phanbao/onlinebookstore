﻿namespace OnlineBookStore.AppService.DTOs.Configuration
{
    public class JwtConfigDto
    {
        public string JwtKey { get; set; }

        public string JwtIssuer { get; set; }

        public string JwtExpireDays { get; set; }
    }
}
