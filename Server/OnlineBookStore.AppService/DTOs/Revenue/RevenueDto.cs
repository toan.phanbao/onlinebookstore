using OnlineBookStore.AppService.DTOs.Receipt;

namespace OnlineBookStore.AppService.DTOs.Revenue
{
    public class RevenueDto: IReportDto
    {
        public int Month { get; set; }
        public decimal Total { get; set; }
        public string DisplayMonth { get; set; }
    }
}