﻿using System;

namespace OnlineBookStore.AppService.DTOs.Issue
{
    public class IssueDto
    {
        public Guid Id { get; set; }

        public decimal Total { get; set; }

        public string ReceivedDate { get; set; }

    }
}