﻿using System.Collections.Generic;

namespace OnlineBookStore.AppService.DTOs.Issue
{
    public class IssueCreateDto
    {
        public List<IssueDetailDto> IssueDetails { get; set; }
    }
}