﻿using System;

namespace OnlineBookStore.AppService.DTOs.Issue
{
    public class IssueDetailDto
    {
        public Guid BookId { get; set; }

        public long Quantity { get; set; }

        public Guid IssueId { get; set; }

        public decimal UnitPrice { get; set; }
    }
}