using System;
using System.Collections.Generic;

namespace OnlineBookStore.AppService.DTOs.Issue
{
    public class IssueViewDto
    {
        public Guid Id { get; set; }

        public decimal Total { get; set; }

        public string ReceivedDate { get; set; }

        public List<IssueDetailViewDto> IssueDetails { get; set; }
    }
}