﻿namespace OnlineBookStore.AppService.DTOs.Publisher
{
    public class PublisherCreateDto
    {
        public string Name { get; set; }

        public string Country { get; set; }
    }
}
