﻿using System;

namespace OnlineBookStore.AppService.DTOs.Publisher
{
    public class PublisherUpdateDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public bool Deleted { get; set; }
    }
}