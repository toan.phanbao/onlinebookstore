﻿using System;

namespace OnlineBookStore.AppService.DTOs.Publisher
{
    public class PublisherDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public bool Deleted { get; set; }
    }
}