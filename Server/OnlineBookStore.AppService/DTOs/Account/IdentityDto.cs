﻿using System.Security.Principal;

namespace OnlineBookStore.AppService.DTOs.Account
{
    public class IdentityDto: IIdentity
    {
        public string AuthenticationType { get; set; }

        public bool IsAuthenticated { get; set; }

        public string Name { get; set; }
    }
}
