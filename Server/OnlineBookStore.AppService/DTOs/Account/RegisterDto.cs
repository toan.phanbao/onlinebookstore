﻿using System.ComponentModel.DataAnnotations;

namespace OnlineBookStore.AppService.DTOs.Account
{
    public class RegisterDto
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 6)]
        public string Password { get; set; }
    }
}
