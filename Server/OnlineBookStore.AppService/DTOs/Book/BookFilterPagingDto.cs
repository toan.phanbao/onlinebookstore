﻿using System;

using Newtonsoft.Json;

using OnlineBookStore.Infrastructure.Paging;

namespace OnlineBookStore.AppService.DTOs.Book
{
    public class BookFilterPagingDto : PagingDto
    {
        [JsonProperty]
        private readonly string deleted_like;

        public bool? Deleted =>
            string.Equals(deleted_like, "Yes", StringComparison.CurrentCultureIgnoreCase)
                ? true
                : string.Equals(deleted_like, "No", StringComparison.CurrentCultureIgnoreCase)
                    ? false
                    : null as bool?;

        [JsonProperty]
        private readonly string title_like;
        public string Title => title_like;

        [JsonProperty]
        private readonly string currency_like;
        public string Currency => currency_like;

        [JsonProperty]
        private readonly string author_like;
        public string Author => author_like;

        [JsonProperty]
        private readonly string genre_like;
        public string Genre => genre_like;

        [JsonProperty]
        private readonly string price_like;
        public string Price => genre_like;
    }
}