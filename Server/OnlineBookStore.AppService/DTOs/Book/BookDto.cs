﻿using System;

namespace OnlineBookStore.AppService.DTOs.Book
{
    public class BookDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public decimal? Price { get; set; }

        public string Currency { get; set; }

        //public string ImageUrl { get; set; }

        public DateTime? PublicationDate { get; set; }

        public string Author { get; set; }

        //public Guid GenreId { get; set; }

        public string Genre { get; set; }

        public string Publisher { get; set; }

        public bool Deleted { get; set; }
    }
}