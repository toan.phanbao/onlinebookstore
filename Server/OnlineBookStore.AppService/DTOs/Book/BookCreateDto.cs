﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OnlineBookStore.AppService.DTOs.Book
{
    public class BookCreateDto
    {
        [Required]
        [MaxLength(512)]
        public string Title { get; set; }

        public decimal? Price { get; set; }

        public string Currency { get; set; }

        public string ImageUrl { get; set; }

        public DateTime? PublicationDate { get; set; }

        public Guid? InventoryId { get; set; }

        public Guid? AuthorId { get; set; }

        public Guid? GenreId { get; set; }

        public Guid? PublisherId { get; set; }

        public string Description { get; set; }
    }
}