﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineBookStore.AppService.Migrations
{
    public partial class Addnewentities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Book_InventoryId",
                table: "Book");

            migrationBuilder.AddColumn<Guid>(
                name: "BookId",
                table: "Inventory",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Inventory_BookId",
                table: "Inventory",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_Book_InventoryId",
                table: "Book",
                column: "InventoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Inventory_Book_BookId",
                table: "Inventory",
                column: "BookId",
                principalTable: "Book",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Inventory_Book_BookId",
                table: "Inventory");

            migrationBuilder.DropIndex(
                name: "IX_Inventory_BookId",
                table: "Inventory");

            migrationBuilder.DropIndex(
                name: "IX_Book_InventoryId",
                table: "Book");

            migrationBuilder.DropColumn(
                name: "BookId",
                table: "Inventory");

            migrationBuilder.CreateIndex(
                name: "IX_Book_InventoryId",
                table: "Book",
                column: "InventoryId",
                unique: true,
                filter: "[InventoryId] IS NOT NULL");
        }
    }
}
