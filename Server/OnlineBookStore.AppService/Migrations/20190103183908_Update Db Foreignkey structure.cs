﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineBookStore.AppService.Migrations
{
    public partial class UpdateDbForeignkeystructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Book_Inventory_InventoryId1",
                table: "Book");

            migrationBuilder.DropForeignKey(
                name: "FK_Inventory_Book_Id",
                table: "Inventory");

            migrationBuilder.DropIndex(
                name: "IX_Book_InventoryId1",
                table: "Book");

            migrationBuilder.RenameColumn(
                name: "InventoryId1",
                table: "Book",
                newName: "InventoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Book_InventoryId",
                table: "Book",
                column: "InventoryId",
                unique: true,
                filter: "[InventoryId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Book_Inventory_InventoryId",
                table: "Book",
                column: "InventoryId",
                principalTable: "Inventory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Book_Inventory_InventoryId",
                table: "Book");

            migrationBuilder.DropIndex(
                name: "IX_Book_InventoryId",
                table: "Book");

            migrationBuilder.RenameColumn(
                name: "InventoryId",
                table: "Book",
                newName: "InventoryId1");

            migrationBuilder.CreateIndex(
                name: "IX_Book_InventoryId1",
                table: "Book",
                column: "InventoryId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Book_Inventory_InventoryId1",
                table: "Book",
                column: "InventoryId1",
                principalTable: "Inventory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Inventory_Book_Id",
                table: "Inventory",
                column: "Id",
                principalTable: "Book",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
