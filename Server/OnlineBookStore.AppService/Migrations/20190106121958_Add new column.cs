﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineBookStore.AppService.Migrations
{
    public partial class Addnewcolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "UnitPrice",
                table: "ReceiptDetail",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Total",
                table: "Receipt",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "UnitPrice",
                table: "IssueDetail",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Total",
                table: "Issue",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UnitPrice",
                table: "ReceiptDetail");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "Receipt");

            migrationBuilder.DropColumn(
                name: "UnitPrice",
                table: "IssueDetail");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "Issue");
        }
    }
}
