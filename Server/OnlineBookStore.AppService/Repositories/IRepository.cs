﻿using Microsoft.EntityFrameworkCore;

using OnlineBookStore.AppService.Domains;

namespace OnlineBookStore.AppService.Repositories
{
    public interface IRepository<TEntity>: IGenericRepository<TEntity, DbContext>
        where TEntity: BaseEntity
    {

    }
}
