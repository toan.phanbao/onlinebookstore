﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using OnlineBookStore.AppService.Domains;

namespace OnlineBookStore.AppService.Repositories
{
    public interface IGenericRepository<TEntity, TContext>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        TContext DbContext { get; }

        DbSet<TEntity> DbSet { get; }

        //IAsyncEnumerable<TEntity> GetAllAsync(Func<TEntity, bool> predicate);

        //Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> expression);

        //IAsyncEnumerable<TEntity> FindAsync(Func<TEntity, bool> predicate);

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        Task<int> SaveChangesAsync();
    }
}