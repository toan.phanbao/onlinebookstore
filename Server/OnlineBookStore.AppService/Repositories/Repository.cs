﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

using OnlineBookStore.AppService.Contexts;
using OnlineBookStore.AppService.Domains;
using OnlineBookStore.AppService.DTOs.Account;

namespace OnlineBookStore.AppService.Repositories
{
    public class Repository<TEntity>: IRepository<TEntity>
        where TEntity: BaseEntity
    {
        private readonly IdentityDto _identity;

        public Repository(BookstoreDbContext dbContext
        , IHttpContextAccessor httpContextAccessor)
        {
            DbContext = dbContext;
            _identity = httpContextAccessor.HttpContext.User.Identity as IdentityDto;
        }

        public DbContext DbContext { get; }

        public DbSet<TEntity> DbSet => DbContext.Set<TEntity>();

        public IAsyncEnumerable<TEntity> GetAllAsync(Func<TEntity, bool> predicate)
        {
            var result = DbSet.ToAsyncEnumerable().Where(predicate);

            return result;
        }

        public async Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> expression)
        {
            var result = DbSet.FirstOrDefaultAsync(expression);

            return await result;
        }

        public IAsyncEnumerable<TEntity> FindAsync(Func<TEntity, bool> predicate)
        {
            var result = DbSet.ToAsyncEnumerable().Where(predicate);

            return result;
        }

        public void Add(TEntity entity)
        {
            AddAuditDataOnCreate(entity);
            DbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
        }

        public void Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await DbContext.SaveChangesAsync();
        }

        private void AddAuditDataOnCreate(TEntity entity)
        {
            entity.Deleted = false;
            entity.CreatedDate = DateTime.UtcNow;
            entity.CreatedBy = _identity?.Name;
        }
    }
}
