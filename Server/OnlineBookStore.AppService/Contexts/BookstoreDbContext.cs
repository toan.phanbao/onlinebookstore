﻿using Microsoft.EntityFrameworkCore;
using OnlineBookStore.AppService.Domains;

namespace OnlineBookStore.AppService.Contexts
{
    public class BookstoreDbContext : DbContext
    {
        public BookstoreDbContext(DbContextOptions<BookstoreDbContext> options)
            : base(options)
        {

        }

        public virtual DbSet<BookEntity> Books { get; set; }
        public virtual DbSet<AuthorEntity> Authors { get; set; }
        public virtual DbSet<GenreEntity> Genres { get; set; }
        public virtual DbSet<InventoryEntity> Inventories { get; set; }
        public virtual DbSet<PublisherEntity> Publishers { get; set; }
        public virtual DbSet<ReceiptEntity> Receipts { get; set; }
        public virtual DbSet<ReceiptDetailEntity> ReceiptDetails { get; set; }
        public virtual DbSet<IssueEntity> Issues { get; set; }
        public virtual DbSet<IssueDetailEntity> IssueDetails { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookEntity>()
                .Property(p => p.Price)
                .HasColumnType("decimal(18,2)");

            modelBuilder.Entity<BookEntity>().ToTable("Book");

            modelBuilder.Entity<AuthorEntity>()
                .ToTable("Author");

            modelBuilder.Entity<GenreEntity>()
                .ToTable("Genre");

            modelBuilder.Entity<InventoryEntity>()
                .ToTable("Inventory");

            modelBuilder.Entity<PublisherEntity>()
                .ToTable("Publisher");

            modelBuilder.Entity<ReceiptEntity>()
                .ToTable("Receipt");

            modelBuilder.Entity<ReceiptDetailEntity>()
                .ToTable("ReceiptDetail");

            modelBuilder.Entity<IssueEntity>()
                .ToTable("Issue");

            modelBuilder.Entity<IssueDetailEntity>()
                .ToTable("IssueDetail");
        }
    }
}
