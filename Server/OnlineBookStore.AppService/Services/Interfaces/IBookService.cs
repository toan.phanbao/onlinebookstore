﻿using System.Threading.Tasks;
using OnlineBookStore.AppService.DTOs.Book;

namespace OnlineBookStore.AppService.Services.Interfaces
{
    public interface IBookService: IBaseService
    {
        Task<BookDto> Find(BookFilterDto filterDto);
    }
}