﻿using System.Linq;
using System.Threading.Tasks;

using AutoMapper;
using AutoMapper.QueryableExtensions;

using Microsoft.EntityFrameworkCore;

using OnlineBookStore.AppService.Domains;
using OnlineBookStore.AppService.DTOs.Book;
using OnlineBookStore.AppService.Repositories;
using OnlineBookStore.AppService.Services.Interfaces;

namespace OnlineBookStore.AppService.Services.Implements
{
    public class BookService : IBookService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<BookEntity> _bookRepository;

        public BookService(
            IMapper mapper
            , IRepository<BookEntity> bookRepository)
        {
            _mapper = mapper;
            _bookRepository = bookRepository;
        }

        public async Task<BookDto> Find(BookFilterDto filterDto)
        {
            var result = await _bookRepository.DbSet
                    .Where(x => 
                        !x.Deleted
                        && (string.IsNullOrWhiteSpace(filterDto.Name)
                            || x.Name == filterDto.Name)
                        )
                    .ProjectTo<BookDto>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync();

            return result;
        }
    }
}