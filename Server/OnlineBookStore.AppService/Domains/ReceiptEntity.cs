﻿using System;
using System.Collections.Generic;

namespace OnlineBookStore.AppService.Domains
{
    public class ReceiptEntity: BaseEntity
    {
        public DateTime ReceivedDate { get; set; }

        public decimal Total { get; set; }

        public virtual ICollection<ReceiptDetailEntity> ReceiptDetails { get; set; }
    }
}