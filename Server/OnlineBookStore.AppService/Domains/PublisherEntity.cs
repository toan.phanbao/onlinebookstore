﻿using System.Collections.Generic;

namespace OnlineBookStore.AppService.Domains
{
    public class PublisherEntity: BaseEntity
    {
        public string Name { get; set; }

        public string Country { get; set; }

        public virtual ICollection<BookEntity> Books { get; set; }
    }
}
