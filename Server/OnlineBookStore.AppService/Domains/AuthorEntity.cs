﻿using System.Collections.Generic;

namespace OnlineBookStore.AppService.Domains
{
    public class AuthorEntity: BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual ICollection<BookEntity> Books { get; set; }
    }
}
