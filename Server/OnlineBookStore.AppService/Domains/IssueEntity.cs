﻿using System;
using System.Collections.Generic;

namespace OnlineBookStore.AppService.Domains
{
    public class IssueEntity: BaseEntity
    {
        public DateTime DeliveredDate { get; set; }

        public decimal Total { get; set; }

        public virtual ICollection<IssueDetailEntity>  IssueDetails { get; set; }
    }
}