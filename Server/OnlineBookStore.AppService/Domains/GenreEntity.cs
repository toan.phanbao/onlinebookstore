﻿using System.Collections.Generic;

namespace OnlineBookStore.AppService.Domains
{
    public class GenreEntity:BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<BookEntity> Books { get; set; }
    }
}
