﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineBookStore.AppService.Domains
{
    public class BookEntity: BaseEntity
    {
        public string Title { get; set; }

        public decimal? Price { get; set; }

        public string Currency { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public DateTime? PublicationDate { get; set; }

        public Guid? InventoryId { get; set; }

        [ForeignKey("InventoryId")]
        public virtual InventoryEntity Inventory { get; set; }

        public Guid? AuthorId { get; set; }

        [ForeignKey("AuthorId")]
        public virtual AuthorEntity Author { get; set; }

        public Guid? GenreId { get; set; }

        [ForeignKey("GenreId")]
        public virtual GenreEntity Genre { get; set; }

        public Guid? PublisherId { get; set; }

        [ForeignKey("PublisherId")]
        public virtual PublisherEntity Publisher { get; set; }
    }
}
