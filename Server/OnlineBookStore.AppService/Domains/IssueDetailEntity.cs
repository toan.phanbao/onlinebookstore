﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineBookStore.AppService.Domains
{
    public class IssueDetailEntity : BaseEntity
    {
        public Guid BookId { get; set; }

        [ForeignKey("BookId")]
        public virtual BookEntity Book { get; set; }

        public long Quantity { get; set; }

        public Guid IssueId { get; set; }

        [ForeignKey("IssueId")]
        public virtual IssueEntity Issue { get; set; }

        public decimal UnitPrice { get; set; }
    }
}