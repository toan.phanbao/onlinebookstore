﻿using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace OnlineBookStore.AppService.Domains
{
    public class InventoryEntity: BaseEntity
    {
        public Guid BookId { get; set; }

        [ForeignKey("BookId")]
        public virtual BookEntity Book { get; set; }

        public long AvailableQuantity { get; set; }
    }
}
