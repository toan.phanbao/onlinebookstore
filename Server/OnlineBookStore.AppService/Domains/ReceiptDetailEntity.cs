﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineBookStore.AppService.Domains
{
    public class ReceiptDetailEntity: BaseEntity
    {
        public Guid BookId { get; set; }

        [ForeignKey("BookId")]
        public virtual BookEntity Book { get; set; }

        public long Quantity { get; set; }

        public Guid ReceiptId { get; set; }

        [ForeignKey("ReceiptId")]
        public virtual ReceiptEntity Receipt { get; set; }

        public decimal UnitPrice { get; set; }
    }
}