﻿namespace OnlineBookStore.Common.Constants
{
    public class DateTimeFormat
    {
        public static string Default => "dd-MMM-yyyy";
    }
}
