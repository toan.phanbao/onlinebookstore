﻿using Newtonsoft.Json;

namespace OnlineBookStore.Infrastructure.Paging
{
    [JsonObject(MemberSerialization.OptIn)]
    public class PagingDto
    {
        [JsonProperty]
        private readonly int _page;
        public int Page => _page;


        [JsonProperty]
        private readonly int _limit;
        public int Limit => _limit;
    }
}
