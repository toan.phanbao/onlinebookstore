﻿namespace OnlineBookStore.Infrastructure.Results
{
    public interface IExecutionResult
    {
        bool Success { get; }

        string Message { get; }

        object Data { get; }
    }

    public class ExecutionResult : IExecutionResult
    {
        public bool Success => true;
        public string Message { get; set; }
        public object Data { get; set; }
    }

    public class ErrorResult : IExecutionResult
    {
        public bool Success => false;
        public string Message { get; set; }
        public object Data => null;

        public ErrorResult(string message)
        {
            Message = message;
        }
    }
}