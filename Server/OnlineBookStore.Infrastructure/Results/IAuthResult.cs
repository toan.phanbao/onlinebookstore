﻿using System;

namespace OnlineBookStore.Infrastructure.Results
{
    public interface IAuthResult
    {
        bool Success { get; set; }

        string Response { get; set; }

        string Redirect { get; set; }

        string[] Messages { get; set; }

        string Error { get; set; }

        AuthToken Data { get; set; }
    }

    public class AuthResult : IAuthResult
    {
        public bool Success { get; set; }

        public string Response { get; set; }

        public string Redirect { get; set; }

        public string[] Messages { get; set; }

        public string Error { get; set; } 

        public AuthToken Data { get; set; }
    }

    public class AuthToken
    {
        public string Token { get; set; }

        public string OwnerStrategyName { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
