﻿using AutoMapper;
using ElmahCore.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OnlineBookStore.AppService.Contexts;
using OnlineBookStore.AppService.DTOs.Configuration;
using OnlineBookStore.AppService.Mapping;
using OnlineBookStore.Common.Constants;
using OnlineBookStore.WebApi.Extensions;
using Serilog;


namespace OnlineBookStore.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<ConnectionStringDto>(Configuration.GetSection("ConnectionStrings"));
            services.Configure<JwtConfigDto>(Configuration.GetSection("JwtConfig"));

            services.AddDbContext<UserDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("UserContext")));

            var tmp = Configuration.GetConnectionString("BookstoreDbContext");

            services.AddDbContext<BookstoreDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("BookstoreDbContext")));

            services.AddIdentity<IdentityUser, IdentityRole>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 3;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequiredUniqueChars = 0;
                })
                .AddEntityFrameworkStores<UserDbContext>()
                .AddDefaultTokenProviders();

            services.RegisterServices();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddJwtAuthentication(Configuration);

            services.AddSwagger();

            services.AddElmah();

            services.EnableCustomCors();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app
            , IHostingEnvironment env
            , ILoggerFactory loggerFactory
            , UserDbContext userDbContext
            , BookstoreDbContext bookstoreDbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                var swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "My API");
            });

            app.UseCors(Policy.AllowAllHeaders);

            loggerFactory.AddSerilog();

            loggerFactory.AddFile("logs\\log-{Date}.txt");
            
            app.ConfigureExceptionHandler(Log.Logger);

            app.UseElmah();

            app.UseAuthentication();

            app.UseMvc();

            userDbContext.Database.EnsureCreated();

            //bookstoreDbContext.Database.EnsureCreated();
        }
    }
}
