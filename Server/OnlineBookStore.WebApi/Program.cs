﻿using System.IO;

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace OnlineBookStore.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(((context, config) =>
                    {
                        config.SetBasePath(Directory.GetCurrentDirectory());
                    }))
                .UseStartup<Startup>()
                .UseUrls("https://localhost:44387/");
    }
}
