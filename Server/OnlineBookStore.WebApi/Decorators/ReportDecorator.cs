using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using OnlineBookStore.AppService.DTOs.Receipt;

namespace OnlineBookStore.WebApi.Decorators
{
    public class ReportDecorator
    {
        public static void AddMissingMonth<T> (ref List<T> data, int? maxMonth = 12)
             where T: IReportDto, new() {
            
            var result = data.ToList();

            foreach(var empty in Enumerable.Range(1, maxMonth ?? 12)) {
                if (data.All(x => x.Month != empty))
                    result.Add(new T {
                        Month = empty,
                        Total = 0
                    });
            }

            data = result;
        }

        public static void FormatForChart<T> (List<T> data) where T: class, IReportDto {
            data = data.OrderBy(x => x.Month).ToList();

            foreach (var item in data)
            {
                item.DisplayMonth = CultureInfo
                    .GetCultureInfo("en-GB")
                    .DateTimeFormat
                    .GetAbbreviatedMonthName(item.Month);
            }
        }
    }
}