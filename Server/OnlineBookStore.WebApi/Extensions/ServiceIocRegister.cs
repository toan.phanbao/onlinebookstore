﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

using OnlineBookStore.AppService.Repositories;

namespace OnlineBookStore.WebApi.Extensions
{
    public static partial class ServiceCollectionExtension
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services
                .AddTransient(typeof(IRepository<>), typeof(Repository<>))
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                ;
        }
    }
}
