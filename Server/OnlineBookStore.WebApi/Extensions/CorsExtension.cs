﻿using Microsoft.Extensions.DependencyInjection;
using OnlineBookStore.Common.Constants;

namespace OnlineBookStore.WebApi.Extensions
{
    public static partial class ServiceCollectionExtension
    {
        public static IServiceCollection EnableCustomCors(this IServiceCollection services)
        {
            return services.AddCors(options =>
            {
                options.AddPolicy(Policy.AllowAllHeaders,
                    builder =>
                    {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
            });
        }
    }
}
