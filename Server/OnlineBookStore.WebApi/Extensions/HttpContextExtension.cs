﻿using System.Linq;
using System.Web;

using Microsoft.AspNetCore.Http;

using Newtonsoft.Json;

namespace OnlineBookStore.WebApi.Extensions
{
    public static class HttpContextExtension
    {
        public static T ParseQueryStringIntoObject<T>(this HttpContext httpContext)
        {
            var dict = HttpUtility.ParseQueryString(httpContext.Request.QueryString.Value);

            var json = JsonConvert.SerializeObject(
                dict.Cast<string>().ToDictionary(k => k, v => dict[v]));

            var deserializeObject = JsonConvert.DeserializeObject<T>(json);

            return deserializeObject;
        }
    }
}
