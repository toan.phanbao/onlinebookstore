﻿using System.Text;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

using OnlineBookStore.AppService.DTOs.Configuration;

namespace OnlineBookStore.WebApi.Extensions
{
    public static partial class ServiceCollectionExtension
    {
        public static IServiceCollection AddJwtAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(config =>
                {
                    config.RequireHttpsMetadata = false;
                    config.SaveToken = true;
                    config.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = configuration.GetValue<string>("JwtConfig:" + nameof(JwtConfigDto.JwtIssuer)),
                        ValidAudience = configuration.GetValue<string>("JwtConfig:" + nameof(JwtConfigDto.JwtIssuer)),
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(configuration
                                .GetValue<string>("JwtConfig:" + nameof(JwtConfigDto.JwtKey)))),
                        //ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });

            return services;
        }
    }
}
