﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using OnlineBookStore.AppService.Domains;
using OnlineBookStore.AppService.DTOs.Genre;
using OnlineBookStore.AppService.Repositories;
using OnlineBookStore.Infrastructure.Paging;
using OnlineBookStore.Infrastructure.Results;
using OnlineBookStore.WebApi.Extensions;

namespace OnlineBookStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class GenreController : ApiControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<GenreEntity> _genreRepository;

        public GenreController(IRepository<GenreEntity> genreRepository
            , IMapper mapper
        )
        {
            _mapper = mapper;
            _genreRepository = genreRepository;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IExecutionResult> GetAll()
        {
            var result = await _genreRepository.DbSet
                .Where(x => !x.Deleted)
                .ProjectTo<GenreDto>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }

        [HttpGet]
        [Route("GetPaging")]
        public async Task<IEnumerable<GenreDto>> GetPaging()
        {
            var paging = HttpContext.ParseQueryStringIntoObject<PagingDto>();

            var query = _genreRepository.DbSet;

            var totalRecord = query.Count();

            Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
            Response.Headers.Add("X-Total-Count", totalRecord.ToString());

            if (0 == totalRecord)
            {
                return new List<GenreDto>();
            }

            var result = await query
                .ProjectTo<GenreDto>(_mapper.ConfigurationProvider)
                .Skip((paging.Page - 1) * paging.Limit)
                .Take(paging.Limit)
                .ToListAsync();

            return result;
        }

        [HttpPost, Route("Create")]
        public async Task<IExecutionResult> Create(GenreCreateDto dto)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = string.Join(Environment.NewLine
                    , ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));

                return new ErrorResult(errorMessage);
            }

            var entity = _mapper.Map<GenreEntity>(dto);

            _genreRepository.Add(entity);

           var result = await _genreRepository.SaveChangesAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }

        [HttpPost, Route("Update")]
        public async Task<IExecutionResult> Update(GenreUpdateDto dto)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = string.Join(Environment.NewLine
                    , ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));

                return new ErrorResult(errorMessage);
            }

            var entity = _mapper.Map<GenreEntity>(dto);

            _genreRepository.Update(entity);

            var result = await _genreRepository.SaveChangesAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }
    }
}