﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using OnlineBookStore.AppService.DTOs.Account;
using OnlineBookStore.AppService.DTOs.Configuration;
using OnlineBookStore.Infrastructure.Results;

namespace OnlineBookStore.WebApi.Controllers
{
    [Route ("api/[controller]")]
    public class AccountController : ApiControllerBase {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IOptions<JwtConfigDto> _jwtOptions;
        private ILogger<AccountController> _logger;

        public AccountController (
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IOptions<JwtConfigDto> jwtOptions,
            ILogger<AccountController> logger
        ) {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtOptions = jwtOptions;
            _logger = logger;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route ("Login")]
        public async Task<IActionResult> Login ([FromBody] LoginDto model) {
            var result = await _signInManager.PasswordSignInAsync (model.Email, model.Password, false, false);

            if (!result.Succeeded) {
                return BadRequest ();
            }

            var appUser = _userManager.Users.SingleOrDefault (r => r.Email == model.Email);

            var authResult = new AuthResult {
                Success = true,
                Data = new AuthToken {
                Token = await GenerateJwtToken (model.Email, appUser),
                CreatedAt = DateTime.Now,
                OwnerStrategyName = string.Empty
                }
            };

            return Ok (authResult);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterDto model)
        {
            var user = new IdentityUser
            {
                UserName = model.Email,
                Email = model.Email
            };

            var result = await _userManager.CreateAsync (user, model.Password);
            await _userManager.AddToRoleAsync (user, "Admin");

            if (!result.Succeeded)
            {
                // return new AuthResult
                // {
                //     Success = false,
                //     Error = string.Join(Environment.NewLine, result.Errors.Select(x => x.Description))
                // };

                return BadRequest();

            }

            await _signInManager.SignInAsync (user, false);
            var token = await GenerateJwtToken (model.Email, user);

            var authResult = new AuthResult {
                Success = true,
                Data = new AuthToken {
                    Token = token
                }
            };

            //return await Task.FromResult(authResult);
            return Ok(authResult);

        }

        private async Task<string> GenerateJwtToken (string email, IdentityUser user) {
            var userRoles = await _userManager.GetRolesAsync(user);

            var claims = new List<Claim> {
                new Claim (JwtRegisteredClaimNames.Sub, email),
                new Claim (JwtRegisteredClaimNames.Jti, Guid.NewGuid ().ToString ()),
                new Claim (ClaimTypes.NameIdentifier, user.Id)
            };

            //claims.AddRange(userRoles.Select(x => new Claim(ClaimTypes.Role, x)));

            var key = new SymmetricSecurityKey (Encoding.UTF8.GetBytes (_jwtOptions.Value.JwtKey));
            var credentials = new SigningCredentials (key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays (Convert.ToDouble (_jwtOptions.Value.JwtExpireDays));

            var token = new JwtSecurityToken (
                _jwtOptions.Value.JwtIssuer,
                _jwtOptions.Value.JwtIssuer,
                claims,
                expires : expires,
                signingCredentials : credentials
            );

            token.Payload["role"] = userRoles;

            var result = new JwtSecurityTokenHandler ().WriteToken (token);
            return await Task.FromResult(result);
        }
    }
}