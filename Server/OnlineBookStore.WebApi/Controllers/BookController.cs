﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using OnlineBookStore.AppService.Domains;
using OnlineBookStore.AppService.DTOs.Book;
using OnlineBookStore.AppService.Repositories;
using OnlineBookStore.Infrastructure.Results;
using OnlineBookStore.WebApi.Extensions;

namespace OnlineBookStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class BookController : ApiControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<BookEntity> _bookRepository;
        
        public BookController(IRepository<BookEntity> bookRepository
            , IMapper mapper)
        {
            _mapper = mapper;
            _bookRepository = bookRepository;
        }

        [HttpGet]
        [Route("GetPaging")]
        public async Task<IEnumerable<BookDto>> GetPaging()
        {
            var paging = HttpContext.ParseQueryStringIntoObject<BookFilterPagingDto>();

            var query = _bookRepository.DbSet
                .Include(x => x.Author)
                .Include(x => x.Genre)
                .AsQueryable();

            query = paging.Deleted.HasValue
                ? query.Where(q => q.Deleted == paging.Deleted)
                : query;

            query = string.IsNullOrWhiteSpace(paging.Title)
                ? query : query.Where(q => q.Title.Contains(paging.Title, StringComparison.InvariantCultureIgnoreCase));

            query = string.IsNullOrWhiteSpace(paging.Currency)
                ? query : query.Where(q => q.Currency.Contains(paging.Currency));

            query = string.IsNullOrWhiteSpace(paging.Author)
                ? query 
                : query.Where(q => $"{q.Author.FirstName} {q.Author.LastName}".Contains(paging.Author, StringComparison.InvariantCultureIgnoreCase));

            query = string.IsNullOrWhiteSpace(paging.Genre)
                ? query : query.Where(q => q.Genre.Name.Contains(paging.Genre, StringComparison.InvariantCultureIgnoreCase));

            var totalRecord = query.Count();

            Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
            Response.Headers.Add("X-Total-Count", totalRecord.ToString());

            if (0 == totalRecord)
            {
                return new List<BookDto>();
            }

            var result = await query
                .ProjectTo<BookDto>(_mapper.ConfigurationProvider)
                .Skip((paging.Page - 1) * paging.Limit)
                .Take(paging.Limit)
                .ToListAsync();

            return result;
        }

        [HttpPost, Route("Create")]
        public async Task<IExecutionResult> Create(BookCreateDto dto)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = string.Join(Environment.NewLine
                    , ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));

                return new ErrorResult(errorMessage);
            }

            var entity = _mapper.Map<BookEntity>(dto);

            _bookRepository.Add(entity);

            var result = await _bookRepository.SaveChangesAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }

        [HttpPost, Route("Update")]
        public async Task<IExecutionResult> Update(BookDetailDto dto)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = string.Join(Environment.NewLine
                    , ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));

                return new ErrorResult(errorMessage);
            }

            var entity = _mapper.Map<BookEntity>(dto);

            _bookRepository.Update(entity);

            var result = await _bookRepository.SaveChangesAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }

        [HttpPost, Route("Delete")]
        public async Task<IExecutionResult> Delete(BookDetailDto dto)
        {
            var entity = await _bookRepository.DbSet.FirstOrDefaultAsync(x => x.Id == dto.Id);

            if (entity == null)
            {
                return new ErrorResult("No entity found");
            }

            entity.Deleted = true;
            _bookRepository.Update(entity);

            var result = await _bookRepository.SaveChangesAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }

        [HttpGet, Route("Get/{id}")]
        public async Task<IExecutionResult> Get(string id)
        {
            var isGuidValid = Guid.TryParse(id, out var bookGuid);

            if (!isGuidValid)
            {
                return new ErrorResult("Guid is not valid");
            }

            var book = await _bookRepository.DbSet
                .ProjectTo<BookDetailDto>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == bookGuid);

            return new ExecutionResult
            {
                Data = book
            };
        }
    }
}