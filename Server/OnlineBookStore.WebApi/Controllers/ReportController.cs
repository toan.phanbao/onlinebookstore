﻿using System;
using System.Linq;

using AutoMapper;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using OnlineBookStore.AppService.Domains;
using OnlineBookStore.AppService.DTOs.Revenue;
using OnlineBookStore.AppService.Repositories;
using OnlineBookStore.Infrastructure.Results;
using OnlineBookStore.WebApi.Decorators;

namespace OnlineBookStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<ReceiptEntity> _receiptRepository;
        private readonly IRepository<IssueEntity> _issueRepository;


        public ReportController(IRepository<ReceiptEntity> receiptRepository
            , IRepository<IssueEntity> issueRepository
           , IMapper mapper
        )
        {
            _mapper = mapper;
            _receiptRepository = receiptRepository;
            _issueRepository = issueRepository;
        }

        private decimal GetRevenue(Func<ReceiptEntity, bool> receiptPredicate, Func<IssueEntity, bool> issuePredicate)
        {
            var receiptTotal = _receiptRepository.DbSet
                .Where(receiptPredicate)
                .Sum(x => x.Total);

            var issueTotal = _issueRepository.DbSet
                .Where(issuePredicate)
                .Sum(x => x.Total);

            return issueTotal - receiptTotal;
        }

        [HttpGet, AllowAnonymous, Route("GetRevenueByMonth/{month}/{year}")]
        public decimal GetRevenueByMonth(int month, int year)
        {
            var receiptPredicate = new Func<ReceiptEntity, bool>(x =>
               x.ReceivedDate.Month == month
               && x.ReceivedDate.Year == year);

            var issuePredicate = new Func<IssueEntity, bool>(x =>
               x.DeliveredDate.Month == month
               && x.DeliveredDate.Year == year);

            return GetRevenue(receiptPredicate, issuePredicate);
        }

        [HttpGet, AllowAnonymous, Route("GetRevenueReport/{year}")]
        public IExecutionResult GetRevenueReport(int year)
        {
            var receipts = _receiptRepository.DbSet

                //.Where(receiptPredicate)
                .GroupBy(x => x.ReceivedDate.Month)
                .Select(x => new RevenueDto
                {
                    Month = x.Key,
                    Total = x.Sum(y => y.Total)
                })
                .ToList();

            ReportDecorator.AddMissingMonth(ref receipts);

           var issues = _issueRepository.DbSet

                //.Where(receiptPredicate)
                .GroupBy(x => x.DeliveredDate.Month)
                .Select(x => new RevenueDto
                {
                    Month = x.Key,
                    Total = x.Sum(y => y.Total)
                })
                .ToList();

            ReportDecorator.AddMissingMonth(ref issues);

            var result = receipts.Join(
                issues,
                r => r.Month,
                i => i.Month,
                (r, i) => new RevenueDto {
                    Month = r.Month,
                    Total = i.Total - r.Total
                })
                .ToList();

            ReportDecorator.FormatForChart(result);
            return new ExecutionResult
            {
                Data = result
            };
        }
    }
}