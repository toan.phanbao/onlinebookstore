﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using OnlineBookStore.AppService.Domains;
using OnlineBookStore.AppService.DTOs.Author;
using OnlineBookStore.AppService.Repositories;
using OnlineBookStore.Infrastructure.Paging;
using OnlineBookStore.Infrastructure.Results;
using OnlineBookStore.WebApi.Extensions;

namespace OnlineBookStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<AuthorEntity> _authorRepository;

        public AuthorController(IRepository<AuthorEntity> authorRepository
           , IMapper mapper
       )
        {
            _mapper = mapper;
            _authorRepository = authorRepository;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IExecutionResult> GetAll()
        {
            var result = await _authorRepository.DbSet
                .Where(x => !x.Deleted)
                .ProjectTo<AuthorViewDto>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }

        [HttpGet]
        [Route("GetPaging")]
        public async Task<IEnumerable<AuthorDto>> GetPaging()
        {
            var paging = HttpContext.ParseQueryStringIntoObject<PagingDto>();

            var query = _authorRepository.DbSet;

            var totalRecord = query.Count();

            Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
            Response.Headers.Add("X-Total-Count", totalRecord.ToString());

            if (0 == totalRecord)
            {
                return new List<AuthorDto>();
            }

            var result = await query
                .ProjectTo<AuthorDto>(_mapper.ConfigurationProvider)
                .Skip((paging.Page - 1) * paging.Limit)
                .Take(paging.Limit)
                .ToListAsync();

            return result;
        }

        [HttpPost, Route("Create")]
        public async Task<IExecutionResult> Create(AuthorCreateDto dto)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = string.Join(Environment.NewLine
                    , ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));

                return new ErrorResult(errorMessage);
            }

            var entity = _mapper.Map<AuthorEntity>(dto);

            _authorRepository.Add(entity);

            var result = await _authorRepository.SaveChangesAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }

        [HttpPost, Route("Update")]
        public async Task<IExecutionResult> Update(AuthorUpdateDto dto)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = string.Join(Environment.NewLine
                    , ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));

                return new ErrorResult(errorMessage);
            }

            var entity = _mapper.Map<AuthorEntity>(dto);

            _authorRepository.Update(entity);

            var result = await _authorRepository.SaveChangesAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }
    }
}