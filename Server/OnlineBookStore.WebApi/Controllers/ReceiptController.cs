﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;
using AutoMapper.QueryableExtensions;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using OnlineBookStore.AppService.Domains;
using OnlineBookStore.AppService.DTOs.Receipt;
using OnlineBookStore.AppService.Repositories;
using OnlineBookStore.Infrastructure.Paging;
using OnlineBookStore.Infrastructure.Results;
using OnlineBookStore.WebApi.Extensions;

namespace OnlineBookStore.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReceiptController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<ReceiptEntity> _receiptRepository;

        public ReceiptController(IRepository<ReceiptEntity> receiptRepository
            , IMapper mapper
        )
        {
            _mapper = mapper;
            _receiptRepository = receiptRepository;
        }

        [HttpGet]
        [Route("GetPaging")]
        public async Task<IEnumerable<ReceiptDto>> GetPaging()
        {
            var paging = HttpContext.ParseQueryStringIntoObject<PagingDto>();

            var query = _receiptRepository.DbSet.Where(x => !x.Deleted);

            var totalRecord = query.Count();

            Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
            Response.Headers.Add("X-Total-Count", totalRecord.ToString());

            if (0 == totalRecord)
            {
                return new List<ReceiptDto>();
            }

            var result = await query
                .ProjectTo<ReceiptDto>(_mapper.ConfigurationProvider)
                .Skip((paging.Page - 1) * paging.Limit)
                .Take(paging.Limit)
                .ToListAsync();

            return result;
        }

        [HttpPost, Route("Create")]
        public async Task<IExecutionResult> Create(ReceiptCreateDto dto)
        {
            if (!ModelState.IsValid)
            {
                var errorMessage = string.Join(Environment.NewLine
                    , ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));

                return new ErrorResult(errorMessage);
            }

            var entity = _mapper.Map<ReceiptEntity>(dto);

            _receiptRepository.Add(entity);

            var result = await _receiptRepository.SaveChangesAsync();

            return new ExecutionResult
            {
                Data = result
            };
        }

        [HttpGet]
        [Route("Get/{guid}")]
        public async Task<IExecutionResult> Get(string guid)
        {
            var isGuidValid = Guid.TryParse(guid, out var receiptGuid);

            if (!isGuidValid)
            {
                return new ErrorResult("Guid is not valid");
            }

            var receipt = await _receiptRepository.DbSet
                .Where(x => !x.Deleted)
                .ProjectTo<ReceiptViewDto>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == receiptGuid);

            return new ExecutionResult
            {
                Data = receipt
            };
        }
    }
}